# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in H:\Android SDK/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

-keep class com.darkmatter.xploron.business.** { *; }
-keepattributes Exceptions,InnerClasses,Signature,Deprecated,SourceFile,LineNumberTable,*Annotation*,EnclosingMethod
-dontobfuscate

    -keep class org.apache.http.** { *; }
    -dontwarn org.apache.http.**

    -keep class org.apache.commons.** {*;}
    -dontwarn org.apache.commons.**

     -keep class com.google.android.gms.** {*;}
     -dontwarn com.google.android.gms.**


     -keep class com.google.api-client.** {*;}
     -dontwarn com.google.api-client.**

     -keep class com.google.http-client.** {*;}
     -dontwarn com.google.http-client.**

     -keep class com.facebook.android.** {*;}
     -dontwarn com.facebook.android.**

     -keep class com.android.support.** {*;}
     -dontwarn com.android.support.**

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

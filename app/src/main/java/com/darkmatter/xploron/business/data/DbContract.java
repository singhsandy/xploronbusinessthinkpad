//package com.darkmatter.xploron.business;
//
//import android.content.ContentUris;
//import android.net.Uri;
//import android.provider.BaseColumns;
//
///**
// * Created by sandeep on 03-Dec-15.
// */
//public class DbContract {
//
//    public static final String CONTENT_AUTHORITY = "com.bizztap.business";
//    public static final Uri BASE_CONTENT_URI = Uri.parse("content://"+ CONTENT_AUTHORITY);
//
//    public static final String PATH_OWNER = "owner";
//    public static final String PATH_BUSINESS = "business";
//    public static final String PATH_OFFERS = "offers";
//    public static final String PATH_LOCATION = "location";
//
//    public static final class Owner implements BaseColumns {
//        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon()
//                .appendPath(PATH_OWNER).build();
//        public static final String CONTENT_DIR =
//                "vnd.android.cursor.dir/" + CONTENT_AUTHORITY + PATH_OWNER;
//        public static final String CONTENT_ITEM =
//                "vnd.android.cursor.item/" + CONTENT_AUTHORITY + PATH_OWNER;
//        public static final String TABLE_NAME = "owner";
//        public static final String COLUMN_NAME = "name";
//
//        public static final Uri buildOwnerUri(long id) {
//            return ContentUris.withAppendedId(CONTENT_URI, id);
//        }
//    }
//        public static final class Business implements BaseColumns{
//            public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon()
//                    .appendPath(PATH_BUSINESS).build();
//            public static final String CONTENT_DIR =
//                "vnd.android.cursor.dir/"+ CONTENT_AUTHORITY + PATH_BUSINESS;
//            public static final String CONTENT_ITEM =
//                    "vnd.android.cursor.dir/"+CONTENT_AUTHORITY+PATH_BUSINESS;
//            public static final String TABLE_NAME = "business";
//
//            // Date, stored as Text with format yyyy-MM-dd
//            public static final String COLUMN_UPDATED_ON = "updated_on";
//            public static final String COLUMN_BUSINESS_NAME = "business_name";
//            public static final String COLUMN_MOBIES_ARR = "mobiles";
//            public static final String COLUMN_LANDLINES_ARR = "landlines";
//            public static final String COLUMN_WEBSITE = "website";
//            public static final String COLUMN_VERIFIED = "verified";
//            public static final String COLUMN_USER_FAVOURITE = "favourite";
//
//            public static final String COLUMN_OPEN_HRS = "open_hrs";
//            public static final String COLUMN_CLOSE_HRS = "close_hrs";
//            public static final String COLUMN_DAYS = "days";
//
//            public static final String COLUMN_DESC = "description";
//            public static final String COLUMN_EMAIL_ID = "email_id";
//            public static final String COLUMN_IMAGEURL = "image";
//
//            public static final String COLUMN_LOCATION_KEY = "location_id";
//            public static final String COLUMN_OWNER_KEY = "owner_id";
//            public static final String COLUMN_CATEGORY_KEY = "category_key";
//
//            public static Uri buildBusinessUri(long id){
//                return ContentUris.withAppendedId(CONTENT_URI,id);
//            }
//        }
//
//    public static final class Location implements BaseColumns{
//        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon()
//                .appendPath(PATH_LOCATION).build();
//        public static final String CONTENT_DIR =
//                "vnd.android.cursor.dir/"+ CONTENT_AUTHORITY + PATH_LOCATION;
//        public static final String CONTENT_ITEM =
//                "vnd.android.cursor.item/"+CONTENT_AUTHORITY + PATH_LOCATION;
//        public static final String TABLE_NAME = "location";
//        public static final String COL_LAT = "lat";
//        public static final String COL_LNG = "lng";
//        public static final String COL_STREET = "street";
//        public static final String COL_LOCALITY = "locality";
//        public static final String COL_CITY = "city";
//        public static final String COL_STATE = "state";
//        public static final String COL_PIN = "pin";
//
//        public static Uri builLocationUri(long id){
//            return ContentUris.withAppendedId(CONTENT_URI,id);
//        }
//    }
//
//    public static class Offers implements BaseColumns{
//        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon()
//                .appendPath(PATH_OFFERS).build();
//        public static final String CONTENT_DIR =
//                "vnd.android.cursor.dir/"+CONTENT_AUTHORITY +PATH_OFFERS;
//        public static final String CONTENT_ITEM =
//                "vnd.android.cursor.item/" + CONTENT_AUTHORITY +PATH_OFFERS;
//        public static final String TABLE_NAME = "offers";
//        public static final String COL_DESC = "description";
//        public static final String COL_ACTIVE_SINCE = "active_since";
//        public static final String COL_VALID_TILL = "valid_till_date";
//        public static final String COL_IMAGE_URL ="image";
//        public static final String COL_OWNER_KEY = "owner_key";
////        public static final String COL_USER_LIKED = "offer_user_liked";
//
//        public static Uri buildOfferUri(long id){
//            return ContentUris.withAppendedId(CONTENT_URI,id);
//        }
//    }
//}

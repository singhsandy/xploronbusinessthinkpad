package com.darkmatter.xploron.business;


import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.os.ResultReceiver;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.provider.Settings;
import android.provider.SyncStateContract;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.NavUtils;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MultiAutoCompleteTextView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.appspot.darkmatter_xploron.xploron.Xploron;
import com.appspot.darkmatter_xploron.xploron.model.Business;
import com.appspot.darkmatter_xploron.xploron.model.BusinessForm;
import com.appspot.darkmatter_xploron.xploron.model.BusinessGetByOwner;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.json.Json;
import com.google.api.client.json.gson.GsonFactory;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;

public class MainActivity extends AppCompatActivity implements Spinner.OnItemSelectedListener
        , GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    private MultiAutoCompleteTextView brandsAutoComplete;
    public static TextView openingTime;
    public static TextView closingTime;
    int tag = 0;
    private Button createBusiness;
    private ImageButton btnCapturePicture, btnCapturePictureGallery;
    private String businessName, category, brands, desc, mobiles, landlines, emailId, websites, openHrs, closeHrs,
            street, locality, city, state, brandFlagText;
    private String businessId = null;
    private String businessIdFromBundle;
    private String imgDecodableString;
    private String mAddressOutput;
    private static String fileName;
    private int pin;

    boolean isImageUploadedthroughflash = false;
    private boolean brandFlag;
    private EditText businessNameInput, descInput, mobileInput, landlineInput, emailIdInput, websiteInput,
            streetInput, localityInput, cityInput, stateInput, pinInput;
    Context context;
    List<String> listServingUrl = new ArrayList<String>();
    List<String> listBlobKey = new ArrayList<>();
    List<String> listDeletedImageUrl = new ArrayList<String>();
    List<String> listDeletedBlobKey = new ArrayList<>();
    List<String> mobileList = new ArrayList<>();
    List<String> landlineList = new ArrayList<>();
    List<String> openHourList = new ArrayList<>();
    List<String> closeHourList = new ArrayList<>();
    List<ImageView> imageLayoutList = new ArrayList<>();
    List<Double> latitudeList = new ArrayList<>();
    List<Double> longitudeList = new ArrayList<>();
    List<Float> accuracyList = new ArrayList<>();
    private GoogleApiClient googleApiClient;
    private Bitmap bitmap;
    private Display display;
    private Uri fileuri, selectedImage;
    private Spinner categorySpinner;

    private GoogleAccountCredential credentials;
    private SharedPreferences prefs;
    private ProgressDialog progressDialog;
    private LinearLayout selectedPhotoContainer;
    private HorizontalScrollView horizontalScrollView;
    private boolean gps_enabled = false;
    private boolean network_enabled = false;
    //    private AddressResultReceiver mResultReceiver;
    protected boolean mAddressRequested;
    private Bundle bundlefromHomeActivity;
    private double longitude, latitude;
    protected LocationRequest mLocationRequest;
    protected Location physicalLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;
        btnCapturePicture = (ImageButton) findViewById(R.id.btncameraUpload);
        btnCapturePictureGallery = (ImageButton) findViewById(R.id.btngalleryUpload);
        selectedPhotoContainer = (LinearLayout) findViewById(R.id.selected_photos_container);
        horizontalScrollView = (HorizontalScrollView) findViewById(R.id.hori_scroll_view);
        categorySpinner = (Spinner) findViewById(R.id.category_spinner);
        businessNameInput = (EditText) findViewById(R.id.business_name);
        descInput = (EditText) findViewById(R.id.business_desc);
        openingTime = (TextView) findViewById(R.id.open_time);
        closingTime = (TextView) findViewById(R.id.close_time);
        mobileInput = (EditText) findViewById(R.id.mbl_number);
        landlineInput = (EditText) findViewById(R.id.landline_number);
        emailIdInput = (EditText) findViewById(R.id.email);
        websiteInput = (EditText) findViewById(R.id.website);
        streetInput = (EditText) findViewById(R.id.street);
        localityInput = (EditText) findViewById(R.id.locality);
        cityInput = (EditText) findViewById(R.id.city);
        stateInput = (EditText) findViewById(R.id.state);
        pinInput = (EditText) findViewById(R.id.pin);
        // mobileInput.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
        ArrayAdapter<String> categoriesAdapter = new ArrayAdapter<String>(context,
                android.R.layout.simple_spinner_item, Commons.categories);
        categoriesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        categorySpinner.setAdapter(categoriesAdapter);
        categorySpinner.setOnItemSelectedListener(CategoryListItemSelectedListener);
        brandsAutoComplete = (MultiAutoCompleteTextView) findViewById(R.id.brands_autocomplete);
        brandsAutoComplete.setOnTouchListener(new BrandsAutocompleteTouchListener());

        openingTime = (TextView) findViewById(R.id.open_time);
        closingTime = (TextView) findViewById(R.id.close_time);

//        mResultReceiver = new AddressResultReceiver(new Handler());

        openingTime.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                DialogFragment newFragment = new OpenTimePickerFragment();
                newFragment.show(getSupportFragmentManager(), "opening_timePicker");
                return false;
            }
        });

        closingTime.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                DialogFragment newFragment = new CloseTimePickerFragment();
                newFragment.show(getSupportFragmentManager(), "closing_timePicker");
                return false;
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        createBusiness = (Button) findViewById(R.id.create_business);
        createBusiness.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                businessName = businessNameInput.getText().toString();
                brands = brandsAutoComplete.getText().toString();
                desc = descInput.getText().toString();
                mobiles = mobileInput.getText().toString();
                landlines = landlineInput.getText().toString();
                emailId = emailIdInput.getText().toString();
                websites = websiteInput.getText().toString();
                openHrs = openingTime.getText().toString();
                closeHrs = closingTime.getText().toString();
                street = streetInput.getText().toString();
                locality = localityInput.getText().toString();
                city = cityInput.getText().toString();
                state = stateInput.getText().toString();
                mobileList = Arrays.asList(mobiles.split(",\\s*"));
                landlineList = Arrays.asList(landlines.split(",\\s*"));
                if (pinInput.getText().toString().length() > 0) {
                    pin = Integer.parseInt(pinInput.getText().toString());
                }
                boolean fieldsOK = validate(new EditText[]{businessNameInput,
                        descInput, streetInput, localityInput, cityInput, stateInput, pinInput});

                String[] brandList = brands.split(",\\s*");
                openHourList.add(openHrs);
                closeHourList.add(closeHrs);
                if (brandList.length > 1) {
                    brandFlagText = "Multi_Brand_Outlet";
                } else {
                    brandFlagText = "Single_Brand_Outlet";
                }
                removeDeletedImageUrlsFromServingUrl();

                Boolean validEmail;
                Matcher matcher = Commons.VALID_EMAIL_ADDRESS_REGEX.matcher(emailId);
                if (matcher.matches() || !(emailId != null && !emailId.isEmpty())) {
                    validEmail = true;
                } else {
                    validEmail = false;
                }
                if (!validEmail) {
                    fieldsOK = false;
                    emailIdInput.setError("Email Not in correct format");
                }

                Boolean validUrl;
                if (android.util.Patterns.WEB_URL.matcher(websites).matches() || !(websites != null && !websites.isEmpty())) {
                    validUrl = true;
                } else {
                    validUrl = false;
                }
                if (!validUrl) {
                    fieldsOK = false;
                    websiteInput.setError("Website in incorrect format");
                }

                Boolean validMobileNumber = false;
                for (Iterator<String> iterator = mobileList.iterator(); iterator.hasNext(); ) {
                    String mobileString = iterator.next();
                    if (mobileString.matches("^[789]\\d{9}$") || !(mobileString != null && !mobileString.isEmpty())) {
//                    if(android.util.Patterns.PHONE.matcher(mobileString).matches()){
                        validMobileNumber = true;
                    } else {
                        validMobileNumber = false;
                        break;
                    }
                }
                if (!validMobileNumber) {
                    fieldsOK = false;
                    mobileInput.setError("Mobile No. in invalid format");
                }

                Boolean validPhoneNumber = false;
                for (Iterator<String> iterator = landlineList.iterator(); iterator.hasNext(); ) {
                    String landlineString = iterator.next();
                    if (landlineString.matches("\\d{11}") || !(landlineString != null && !landlineString.isEmpty())) {
                        validPhoneNumber = true;
                    } else {
                        validPhoneNumber = false;
                        break;
                    }
                }
                if (!validPhoneNumber) {
                    fieldsOK = false;
                    landlineInput.setError("Landline No. in invalid format");
                }
                if (fieldsOK) {
                    new MakePostTask(businessIdFromBundle).execute();
                } else {
                    Toast.makeText(MainActivity.this, "Please fill correctly all edit box", Toast.LENGTH_SHORT).show();
                    return;
                }
            }
        });

        display = getWindowManager().getDefaultDisplay();
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String emailIdfromSharedPreference = prefs.getString(Commons.EMAIL, "sandeepkumarsingh.1993@gmail.com");
        credentials = GoogleAccountCredential.usingAudience(this,
                "server:client_id:454280588675-33ro5ieerek0mcfgkubrk7aqpfbivjge.apps.googleusercontent.com");
        credentials.setSelectedAccountName(emailIdfromSharedPreference);
        btnCapturePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                captureImage();
            }
        });

        btnCapturePictureGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                captureImageThroughGallery();
            }
        });

        bundlefromHomeActivity = getIntent().getExtras();
        if (bundlefromHomeActivity != null) {
            String businessStringValue = bundlefromHomeActivity.getString("BusinessDescription");
            createBusiness.setText("Update Business");
            setTitle("Update Business");
            try {
                JSONObject jsonObject = new JSONObject(businessStringValue);
                businessIdFromBundle = jsonObject.getString("businessId");
                businessNameInput.setText(jsonObject.getString("businessName"));
                for (int i = 0; i < Commons.categories.length; i++) {
                    if (jsonObject.getString("category").equals(categorySpinner.getItemAtPosition(i))) {
                        categorySpinner.setSelection(i);
                    }
                }
                latitude = Double.valueOf(jsonObject.getString("latitude"));
                longitude = Double.valueOf(jsonObject.getString("longitude"));
                if(jsonObject.has("brandList"))
                {
                    brandsAutoComplete.setText(jsonObject.getString("brandList"));
                }
                descInput.setText(jsonObject.getString("description"));
                JSONArray openingTimeJsonArray = (JSONArray) jsonObject.get("openingTime");
                List<String> openingTimeStringList = new ArrayList<>();
//                for(int i=0;i<openingTimeJsonArray.length();i++)
//                {
//                    String openingTime=openingTimeJsonArray.getString(i);
//                    openingTimeStringList.add(openingTime);
//                }
                String openingTimeString = openingTimeJsonArray.getString(0);
                openingTime.setText(openingTimeString);
                JSONArray closingTimeJsonArray = (JSONArray) jsonObject.get("closingTime");
                String closingTimeString = closingTimeJsonArray.getString(0);
                closingTime.setText(closingTimeString);
                if(jsonObject.has("mobileNumber")) {
                    JSONArray mobileStringJsonArray = (JSONArray) jsonObject.get("mobileNumber");
                    StringBuilder mobileString = new StringBuilder();
                    for (int i = 0; i < mobileStringJsonArray.length(); i++) {
                        String mobile = mobileStringJsonArray.getString(i);
                        mobileString.append(mobile).append(",");
                    }
                    int mobileStringLength = mobileString.length();
                    mobileInput.setText(mobileString.substring(0, mobileStringLength - 1));
                }
                if(jsonObject.has("landlineNumber")) {
                    JSONArray landlineStringJsonArray = (JSONArray) jsonObject.get("landlineNumber");
                    StringBuilder landlineString = new StringBuilder();
                    for (int i = 0; i < landlineStringJsonArray.length(); i++) {
                        String landline = landlineStringJsonArray.getString(i);
                        landlineString.append(landline).append(",");
                    }
                    int landlineStringLength = landlineString.length();
                    landlineInput.setText(landlineString.substring(0, landlineStringLength - 1));
                }
                if(jsonObject.has("storeOwnerEmail")) {
                    emailIdInput.setText(jsonObject.getString("storeOwnerEmail"));
                }
                if(jsonObject.has("website")) {
                    websiteInput.setText(jsonObject.getString("website"));
                }
                streetInput.setText(jsonObject.getString("street"));
                localityInput.setText(jsonObject.getString("locality"));
                cityInput.setText(jsonObject.getString("city"));
                stateInput.setText(jsonObject.getString("state"));
                pinInput.setText(String.valueOf(jsonObject.getInt("pin")));

                if (jsonObject.has("listUrlImages")) {
                    JSONArray jsonArray = jsonObject.getJSONArray("listUrlImages");
                    JSONArray jsonArrayblob = jsonObject.getJSONArray("listBlobkeys");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        listServingUrl.add(jsonArray.getString(i));
                        listBlobKey.add(jsonArrayblob.getString(i));
                        new urlToBitmapImage().execute(jsonArray.getString(i));
                        Log.e("bitmapFactory", bitmap + "");
                    }
                }
            } catch (Exception ex) {
                Log.e("Exception", ex + " JSON exception");
            }
        } else {
            createBusiness.setText("Create Business");
            setTitle("Create Business");
        }
        createLocationRequest();
        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(MainActivity.this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        mAddressRequested = false;
        mAddressOutput = "";
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(40000);
//        mLocationRequest.setFastestInterval(20000);
        mLocationRequest.setNumUpdates(5);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!googleApiClient.isConnecting() || !googleApiClient.isConnected()) {
            googleApiClient.connect();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (googleApiClient.isConnecting() || googleApiClient.isConnected()) {
            googleApiClient.disconnect();
        }
    }

    @Override
    protected void onResume(){
        LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }
        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }
        if (!gps_enabled && !network_enabled) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(context);
            dialog.setMessage("GPS Network not enabled");
            dialog.setPositiveButton("Open Location Setting", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    context.startActivity(myIntent);
                }
            });
            dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    MainActivity.this.finish();
                }
            });
            dialog.show();
        }
        super.onResume();
    }

    @Override
    public void onConnected(Bundle bundle) {
        if (bundlefromHomeActivity == null) {
            physicalLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
            if(physicalLocation!=null) {
                float accuracy = physicalLocation.getAccuracy();
                Double longitudeByGetLastLocation, latitudeByGetLastLocation;
                longitudeByGetLastLocation = physicalLocation.getLongitude();
                latitudeByGetLastLocation = physicalLocation.getLatitude();
                latitudeList.add(latitudeByGetLastLocation);
                longitudeList.add(longitudeByGetLastLocation);
                accuracyList.add(accuracy);
                Log.e("GetLastLocation", longitude + " " + latitude + " " + accuracy);
            }
            startLocationUpdates();
        }
    }

    protected void startLocationUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(
                googleApiClient, mLocationRequest, this);

        Log.d(TAG, "Location update started ..............: ");
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "Firing onLocationChanged..............................................");
        if(location!= null) {
            Double latitudeOnLocaitonChange,longitudeOnLocationChange;
            physicalLocation = location;
            latitudeOnLocaitonChange = physicalLocation.getLatitude();
            longitudeOnLocationChange = physicalLocation.getLongitude();
            float accuracy = physicalLocation.getAccuracy();
            latitudeList.add(latitudeOnLocaitonChange);
            longitudeList.add(longitudeOnLocationChange);
            accuracyList.add(accuracy);
            Log.e("LocationUpdates", latitude + " "+ longitude + " " + accuracy);
        }
//        mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
    }


    @Override
    protected void onPause() {
        super.onPause();
        if (bundlefromHomeActivity == null) {
            stopLocationUpdates();
        }
    }

    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                googleApiClient, this);
        Log.d(TAG, "Location update stopped .......................");
    }

    @Override
    public void onConnectionSuspended(int i) {
        googleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        //TODO location not found error handling
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        //if(position != 0) isMultiBranded = true;
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Commons.CAMERA_CAPTURE_IMAGE_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    isImageUploadedthroughflash = true;
                    previewMedia(true);
                } else if (resultCode == RESULT_CANCELED) {
                    Toast.makeText(getApplicationContext(),
                            "User cancelled image capture", Toast.LENGTH_SHORT)
                            .show();
                } else {
                    // failed to capture image
                    Toast.makeText(getApplicationContext(),
                            "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                            .show();
                }
                break;

            case Commons.GALLERY_CAPTURE_IMAGE_REQUEST_CODE:
                if (resultCode == RESULT_OK && data != null) {
                    selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    // Get the cursor
                    Cursor cursor = getContentResolver().query(selectedImage,
                            filePathColumn, null, null, null);
                    // Move to first row
                    cursor.moveToFirst();
                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    imgDecodableString = cursor.getString(columnIndex);
                    cursor.close();
                    isImageUploadedthroughflash = false;
                    previewMedia(true);
                } else {
                    Toast.makeText(this, "You haven't picked Image",
                            Toast.LENGTH_LONG).show();
                }
                break;
            case Commons.REQUEST_CODE_DELETE_IMAGE:
                if (resultCode == RESULT_OK) {
                    int selected_index = data.getIntExtra("Position", 0);
                    imageLayoutList.get(selected_index).setVisibility(View.GONE);
                    listDeletedImageUrl.add(listServingUrl.get(selected_index));
                    listDeletedBlobKey.add(listBlobKey.get(selected_index));
                }
//                  else {
//                    Toast.makeText(getApplicationContext(), "Sorry couldn't delete image", Toast.LENGTH_SHORT).show();
//                }
                break;
        }
    }

    private void removeDeletedImageUrlsFromServingUrl() {
        for (Iterator<String> iterator = listDeletedImageUrl.iterator(); iterator.hasNext(); ) {
            String imageurl = iterator.next();
            listServingUrl.remove(imageurl);
        }
        for (Iterator<String> iterator = listDeletedBlobKey.iterator(); iterator.hasNext(); ) {
            String blobUrl = iterator.next();
            listBlobKey.remove(blobUrl);
        }
        Log.e("Length of serving URLs", String.valueOf(listServingUrl.size()));
    }


    private boolean validate(EditText[] fields) {
        for (int i = 0; i < fields.length; i++) {
            EditText currentField = fields[i];
            if (currentField.getText().toString().length() <= 0) {
                return false;
            }
        }
        return true;
    }


    private class BrandsAutocompleteTouchListener implements MultiAutoCompleteTextView.OnTouchListener {

        private final String TAG = BrandsAutocompleteTouchListener.class.getSimpleName();

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            String categories = category;
            Log.e(TAG, "onTouch called categories : " + categories);
            ArrayList<String> brands = BrandsUtil.getBrands(category);
            for (String brand : brands) {
                Log.e(TAG, brand);
            }
            ArrayAdapter<String> brandsAdapter = new ArrayAdapter<String>(context,
                    android.R.layout.simple_dropdown_item_1line, brands);
            brandsAutoComplete.setAdapter(brandsAdapter);
            brandsAutoComplete.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
            return false;
        }
    }

    public static class OpenTimePickerFragment extends DialogFragment
            implements TimePickerDialog.OnTimeSetListener {

        public OpenTimePickerFragment() {
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }

        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            String Minute = String.valueOf(minute);
            if (minute < 10) {
                Minute = "0" + minute;
            }
            if (hourOfDay >= 12) {
                if (hourOfDay > 12) hourOfDay %= 12;

                openingTime.setText(hourOfDay + ":" + Minute + " pm");
            } else {
                openingTime.setText(hourOfDay + ":" + Minute + " am");
            }
        }
    }

    private AdapterView.OnItemSelectedListener CategoryListItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            category = parent.getItemAtPosition(position).toString();
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
//            nothing happen here.
        }
    };

    public static class CloseTimePickerFragment extends DialogFragment
            implements TimePickerDialog.OnTimeSetListener {

        public CloseTimePickerFragment() {
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }

        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            String Minute = String.valueOf(minute);
            if (minute < 10) {
                Minute = "0" + minute;
            }
            if (hourOfDay >= 12) {
                if (hourOfDay > 12) hourOfDay %= 12;

                closingTime.setText(hourOfDay + ":" + Minute + " pm");
            } else {
                closingTime.setText(hourOfDay + ":" + Minute + " am");
            }
        }
    }

    private class MakePostTask extends AsyncTask<Void, Void, BusinessGetByOwner> {

        String businessIdParameter;

        public MakePostTask(String businessId) {
            this.businessIdParameter = businessId;
        }

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(MainActivity.this);
            if (businessIdParameter != null) {
                progressDialog.setMessage("Updating Business...");
            } else {
                progressDialog.setMessage("Creating New Business...");
            }
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
            super.onPreExecute();
        }


        @Override
        protected BusinessGetByOwner doInBackground(Void... params) {
            BusinessGetByOwner response;
            if (bundlefromHomeActivity == null) {
                int minIndex = accuracyList.indexOf(Collections.min(accuracyList));
                latitude = latitudeList.get(minIndex);
                longitude = longitudeList.get(minIndex);
                accuracyList.get(minIndex);
            }
            Xploron.Builder builder = new Xploron.Builder(
                    AndroidHttp.newCompatibleTransport(), new GsonFactory(),
                    credentials);
            builder.setRootUrl("https://darkmatter-xploron.appspot.com/_ah/api/");
            Xploron service = builder.build();
            BusinessForm businessForm = new BusinessForm();
            businessForm.setBusinessName(businessName);
            businessForm.setBrandedFlag(brandFlagText);
            businessForm.setDescription(desc);
            businessForm.setLatitude(latitude);
            businessForm.setLongitude(longitude);
            businessForm.setStreet(street);
            businessForm.setLocality(locality);
            businessForm.setCity(city);
            businessForm.setState(state);
            businessForm.setPin(pin);
            businessForm.setBrandList(brands);
            businessForm.setMobileNumber(mobileList);
            businessForm.setLandlineNumber(landlineList);
            businessForm.setStoreOwnerEmail(emailId);
            businessForm.setWebsite(websites);
            businessForm.setOtherLinks("www.otherlinks.com");
            businessForm.setCategory(category);
            businessForm.setOpeningTime(openHourList);
            businessForm.setClosingTime(closeHourList);
            businessForm.setListUrlImages(listServingUrl);
            businessForm.setListBlobkeys(listBlobKey);
            try {
                if (listDeletedBlobKey.size() > 0) {
                    service.removeImages(listDeletedBlobKey).execute();
                }
                if (businessIdParameter != null) {
                    response = service.updateBusiness(businessIdParameter, businessForm).execute();
                } else {
                    response = service.saveBusiness(businessForm).execute();
                }
                Log.e("BusinessFormResponse", response.toString());
                return response;
            } catch (IOException e) {
                Log.e("IOException", e + "");
                return null;
            } catch (Exception e) {
                Log.e("Exception", e.getMessage());
                return null;
            }
        }

        @Override
        protected void onPostExecute(BusinessGetByOwner result) {
            if (result != null) {
                businessId = result.getBusinessId();
                progressDialog.dismiss();
                if (businessIdParameter != null) {
                    Toast.makeText(getApplicationContext(), "You have successfully Updated this Business", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Congratulation You have Created a new Business.", Toast.LENGTH_SHORT).show();
                }
                Intent intent = new Intent(MainActivity.this, HomeActivity.class);
                startActivity(intent);
            } else {
                Toast.makeText(getApplicationContext(), "There is some problem in creating Business.", Toast.LENGTH_SHORT);
            }
//            alertDialogForOffers();
//            finish();
            super.onPostExecute(result);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        NavUtils.navigateUpFromSameTask(this);
        return super.onKeyDown(keyCode, event);
    }

//    private void alertDialogForOffers() {
//        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
//        dialog.setMessage("Want To Create Offers of this Business.");
//        dialog.setPositiveButton("Open Offers Page", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                Intent offerIntent = new Intent(MainActivity.this, OfferActivity.class);
//                offerIntent.putExtra("BusinessId", businessId);
//                context.startActivity(offerIntent);
//            }
//        });
//        dialog.setNegativeButton("Not Now", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                Intent intent = new Intent(MainActivity.this, HomeActivity.class);
//                startActivity(intent);
//            }
//        });
//        dialog.show();
//
//    }

    private void captureImage() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        fileuri = getOutputMediaFileUri(Commons.MEDIA_TYPE_IMAGE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileuri);

        startActivityForResult(intent, Commons.CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    private void captureImageThroughGallery() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, Commons.GALLERY_CAPTURE_IMAGE_REQUEST_CODE);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("file_uri", fileuri);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        fileuri = savedInstanceState.getParcelable("file_uri");
    }

    private void previewMedia(boolean isImage) {
        // Checking whether captured media is image or video
        if (isImage) {
            LinearLayout.LayoutParams paramsImagePreview = new LinearLayout.LayoutParams(300, 300);
            ImageView imagePreview = new ImageView(this);
            horizontalScrollView.setVisibility(View.VISIBLE);
            paramsImagePreview.setMargins(0, 0, 20, 0);
            imagePreview.setLayoutParams(paramsImagePreview);
            imagePreview.setScaleType(ImageView.ScaleType.FIT_XY);
            imagePreview.setTag(tag);
            tag++;
            imagePreview.setOnClickListener(imagePreviewClickListener);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 8;
            String pathName;
            if (isImageUploadedthroughflash) {
                pathName = fileuri.getPath();
            } else {
                pathName = imgDecodableString;
            }
            bitmap = BitmapFactory.decodeFile(pathName, options);
            imagePreview.setImageBitmap(bitmap);
//                linearLayout.addView(imagePreview);
//                linearLayout.addView(cancelImageButton);
            imageLayoutList.add(imagePreview);
//                selectedPhotoContainer.addView(linearLayout);
            selectedPhotoContainer.addView(imagePreview);
            progressDialog = ProgressDialog.show(this, "", "Loading...");
            new UploadFileToServer().execute();
        }
    }

//    protected void startIntentService() {
//        Intent intent = new Intent(this, FetchAddressIntentService.class);
//        intent.putExtra(Commons.RECEIVER, mResultReceiver);
//        intent.putExtra(Commons.LOCATION_DATA_EXTRA, physicalLocation);
//        startService(intent);
//    }


    private View.OnClickListener imagePreviewClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int selected_index = (Integer) v.getTag();
            String servingUrl = listServingUrl.get(selected_index);
            Intent intent = new Intent(MainActivity.this, FullScreenImageActivity.class);
            intent.putExtra("ImageUrl", servingUrl);
            intent.putExtra("Position", selected_index);
            startActivityForResult(intent, Commons.REQUEST_CODE_DELETE_IMAGE);
        }
    };

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    private static File getOutputMediaFile(int type) {
        File mediaStorageDir = new File(
                Environment.getExternalStoragePublicDirectory
                        (Environment.DIRECTORY_PICTURES), Commons.IMAGE_DIRECTORY_NAME);
        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(TAG, "Oops! Failed create "
                        + Commons.IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == Commons.MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
            fileName = new String("IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }
        return mediaFile;
    }

    private class urlToBitmapImage extends AsyncTask<String, Void, Bitmap> {
        @Override
        protected Bitmap doInBackground(String... param) {
            String imageUrl = param[0];
            InputStream urlInputStream;
            Bitmap bitmapimage;
            try {
                URL url = new URL(imageUrl);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                urlInputStream = connection.getInputStream();
                bitmapimage = BitmapFactory.decodeStream(urlInputStream);
            } catch (Exception ex) {
                bitmapimage = null;
                Log.e("Exception", ex.getMessage());
            }
            Log.e("urlToImage", bitmapimage + "");
            return bitmapimage;
        }

        @Override
        protected void onPostExecute(Bitmap bitmapresult) {
            ImageView imagePreview = new ImageView(MainActivity.this);
            LinearLayout.LayoutParams imagePreviewParams = new LinearLayout.LayoutParams(300, 300);
            horizontalScrollView.setVisibility(View.VISIBLE);
            imagePreviewParams.setMargins(0, 0, 20, 0);
            imagePreview.setTag(tag);
            tag++;
            imageLayoutList.add(imagePreview);
            imagePreview.setOnClickListener(imagePreviewClickListener);
            bitmap = bitmapresult;
            imagePreview.setLayoutParams(imagePreviewParams);
            imagePreview.setImageBitmap(bitmap);
            imagePreview.setScaleType(ImageView.ScaleType.FIT_XY);
            selectedPhotoContainer.addView(imagePreview);
            Log.e("FinalInputStream", bitmap + "");
            super.onPostExecute(bitmapresult);
        }
    }

    private class UploadFileToServer extends AsyncTask<Void, Integer, String> {
        @Override
        protected String doInBackground(Void... params) {
            return uploadFile();
        }

        @SuppressWarnings("deprecation")
        private String uploadFile() {

            MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
//            AndroidMultiPartEntity entity = new AndroidMultiPartEntity()
//            AndroidMultiPartEntity entity = new AndroidMultiPartEntity(HttpMultipartMode.BROWSER_COMPATIBLE,
//                    new ProgressListener() {
//
//                        @Override
//                        public void transferred(long num) {
//                            Log.e("NUMINPROGRESSLISTNER", num + " " + totalSize);
//                            publishProgress((int) ((num * 100 / (float) totalSize)));
//                        }
//                    });
            HttpClient httpClient = new DefaultHttpClient();
            HttpGet httpGet_new = new HttpGet("https://darkmatter-xploron.appspot.com/bloburlget"); // 10.0.2.2 is localhost's IP address in Android emulator
            try {

                HttpResponse response = httpClient.execute(httpGet_new);
                HttpEntity urlEntity = response.getEntity();
                InputStream in = urlEntity.getContent();
                String str = "";
                while (true) {
                    int ch = in.read();
                    if (ch == -1)
                        break;
                    str += (char) ch;
                }
                Log.e("xploron upload url: ", str);
                HttpPost httppost = new HttpPost(str);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                Bitmap bitmap;
                String filePathName;
                if (isImageUploadedthroughflash) {
                    bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), fileuri);
                    filePathName = fileName;
                    Log.e("Xploron: Image Uri is: ", fileuri + "");

                } else {
                    bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImage);
                    filePathName = imgDecodableString;
                }
                bitmap.compress(Bitmap.CompressFormat.JPEG, 50, baos);
                byte[] imageBytes = baos.toByteArray();

                //FileBody fileBody = new FileBody(f);
                ByteArrayBody bab = new ByteArrayBody(imageBytes, filePathName);
            /* example for setting a HttpMultipartMode */
                //builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
                entity.addPart("photo", bab);
                httppost.setEntity(entity);
                response = httpClient.execute(httppost); //Here "uploaded" servlet is automatically       invoked
                urlEntity = response.getEntity(); //Response will be returned by "uploaded" servlet in JSON format
                in = urlEntity.getContent();
                str = "";
                while (true) {
                    int ch = in.read();
                    if (ch == -1)
                        break;
                    str += (char) ch;
                }


                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 200) {
                    // Server response
                    Log.e("Xploron uploaded url: ", str);
                    progressDialog.dismiss();
                } else {
                    str = "Error occurred! Http Status Code: "
                            + statusCode;
                    progressDialog.dismiss();
                }
                return str;
            } catch (ClientProtocolException e) {
                return e.getMessage();
            } catch (IOException e) {
                return e.getMessage();
            }
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                JSONObject obj = new JSONObject(result);
                String servingUrl = obj.getString("servingUrl");
                String servingBlobKey = obj.getString("blobKey");
                listServingUrl.add(servingUrl);
                listBlobKey.add(servingBlobKey);
            } catch (JSONException e) {
                Log.e("Json Exception", "Could not convert string to json");
            }
            // showing the server response in an alert dialog
//            showAlert(result);
            super.onPostExecute(result);
        }
    }

//    class AddressResultReceiver extends ResultReceiver {
//        public AddressResultReceiver(Handler handler) {
//            super(handler);
//        }
//
//
//        @Override
//        protected void onReceiveResult(int resultCode, Bundle resultData) {
//
//            // Display the address string
//            // or an error message sent from the intent service.
//            mAddressOutput = resultData.getString(Commons.RESULT_DATA_KEY);
////            String lines[] = mAddressOutput.split("\n");
//            String cityString = mAddressOutput.split(",")[0];
//            String stateAndPin = mAddressOutput.split(",")[1];
//            String stateString = stateAndPin.split("\\s+")[1];
//            int pinValue = Integer.parseInt(stateAndPin.split("\\s+")[2]);
//            cityInput.setText(cityString);
//            stateInput.setText(stateString);
//            pinInput.setText(String.valueOf(pinValue));
//            Log.e("Address Output", mAddressOutput);
//
//        }
//    }
}


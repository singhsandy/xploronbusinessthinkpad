package com.darkmatter.xploron.business;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.util.Log;

import com.facebook.GraphResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Pattern;

/**
 * Created by sandeep on 01-Dec-15.
 */
public class Commons {
    public static final String USER_NAME = "name";
    public static final String BIRTH_DAY = "birth_day";
    public static final String BIRTH_MONTH = "birth_month";
    public static final String BIRTH_YEAR = "birth_year";
    public static final String EMAIL = "email";
//    public static final String EMAIL_OWNER = "email_owner";
    public static final String PIC_URL = "picture_url";
    public static final String GENDER = "gender";
    public static final String HAS_USER_LOGGED_INTO_GOOGLE_PLUS = "user_logged_in_status";
//    public static final String HAS_USER_NEVER_LOGGGED_IN = "has_user_never_logged_in";

    public static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    public static final int GALLERY_CAPTURE_IMAGE_REQUEST_CODE = 101;
    public static final int REQUEST_CODE_DELETE_IMAGE = 102;
    public static final int MEDIA_TYPE_IMAGE = 103;
    public static final int SUCCESS_RESULT = 0;
    public static final int FAILURE_RESULT = 1;
    public static final String IMAGE_DIRECTORY_NAME = "Android File Upload";
    public static final String PACKAGE_NAME =
            "com.bizztap.businesses";
    public static final String RECEIVER = PACKAGE_NAME + ".RECEIVER";
    public static final String RESULT_DATA_KEY = PACKAGE_NAME +
            ".RESULT_DATA_KEY";
    public static final String LOCATION_DATA_EXTRA = PACKAGE_NAME +
            ".LOCATION_DATA_EXTRA";

    public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    public static String[] categories = {"Clothing","Electronics", "Fashion Accessories", "Footwear",
            "Salon", "Furniture/Home Furnishing", "Gift Shop","Restaurant", "Medicinal Products", "Sports Equipment",
            "Grocery", "Spa", "SuperMarket", "Gym"};


    public static boolean isConnectingToInternet(Context context) {
        ConnectivityManager cm = (ConnectivityManager)context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if(cm != null) {
            NetworkInfo[] networkInfos = cm.getAllNetworkInfo();
            if(networkInfos != null) {
                for (NetworkInfo info: networkInfos) {
                    if(info.getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
    public static void parseFacebookGraphAPIresponse(GraphResponse response, Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        JSONObject user = response.getJSONObject();

        try {
            String name = user.getString("name");
            String email = user.getString("email");
            String gender = user.getString("gender");
            String birthday = user.getString("birthday");
            String pictureUrl = user.getJSONObject("picture").getJSONObject("data").getString("url");

            SharedPreferences.Editor editor = prefs.edit();
            editor.putString(USER_NAME, name);
            editor.putString(EMAIL, email);
            editor.putString(PIC_URL, pictureUrl);
            editor.putString(GENDER, gender);
            editor.putString(BIRTH_DAY, birthday.substring(3,5));
            editor.putString(BIRTH_MONTH, birthday.substring(0,2));
            editor.putString(BIRTH_YEAR, birthday.substring(6,10));
            editor.commit();

            //Log.e("JSON", name + " " + email + " " + gender + " " + birthday + " " + pictureUrl);
        } catch (JSONException e) {
            Log.e("JSON exception", e.getMessage());
        } catch (Exception e) {
            Log.e("Excptn Parse FB rspns", e+"");
        }
    }

}

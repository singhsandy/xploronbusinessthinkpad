package com.darkmatter.xploron.business;

import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;

import org.json.JSONObject;

import java.util.Arrays;

/**
 * Created by sandeep on 01-Dec-15.
 */
public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int RC_SIGN_IN = 0;
    private Context context;
    private SharedPreferences pref;
    private SignInButton btnSignIn;
    private CallbackManager fbCallbackManager;
    private LoginButton fbLoginButton;
    private TextView splashText;
    private GoogleApiClient mGoogleApiClient;
    private ConnectionResult mConnectionResult;
    private boolean mResolvingError = false;
    private boolean doubleBackToExitPressedOnce = false;
    private boolean isUserLogedIntoGoogle;
    private boolean mSignInClicked;
    private FacebookCallback<LoginResult> callback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            //Log.e("Facebook login", "on login success callback");
            GraphRequest request = GraphRequest.newMeRequest (loginResult.getAccessToken(),
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(JSONObject object, GraphResponse response) {
                            // Application code
                            Commons.parseFacebookGraphAPIresponse(response, getApplicationContext());
                            //Log.e("Graph API response", response.toString());
                        }
                    });
            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,name,email,gender,picture,birthday");
            request.setParameters(parameters);
            request.executeAsync();

            SharedPreferences.Editor editor = pref.edit();
            editor.commit();

            Intent intent = new Intent(context, HomeActivity.class);
            startActivity(intent);
            finish();
        }

        @Override
        public void onCancel() {
            // TODO add this message to text view
        }

        @Override
        public void onError(FacebookException e) {
            // TODO add this message to text view
            Log.e("facebook login", e.getMessage() + " " + e);
        }
    };


    public void onCreate(Bundle onSavedInstanceState) {
        super.onCreate(onSavedInstanceState);
        setContentView(R.layout.activity_login);
        splashText = (TextView)findViewById(R.id.splash_loginText);
        context = this;
        btnSignIn = (SignInButton) findViewById(R.id.sign_in_button);
        Typeface face = Typeface.createFromAsset(getAssets(),"fonts/Roboto-Light.ttf");
        splashText.setTypeface(face);
        btnSignIn.setOnClickListener(this);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        fbCallbackManager = CallbackManager.Factory.create();
//        fbLoginButton = (LoginButton) findViewById(R.id.login_button);
//        fbLoginButton.setReadPermissions(Arrays.asList("user_friends", "email",
//                "public_profile", "user_birthday"));
//        fbLoginButton.registerCallback(fbCallbackManager, callback);

        pref = PreferenceManager.getDefaultSharedPreferences(this);

      isUserLogedIntoGoogle = pref.getBoolean(Commons.HAS_USER_LOGGED_INTO_GOOGLE_PLUS, false);

      if(!isUserLogedIntoGoogle){
          mGoogleApiClient = new GoogleApiClient.Builder(this)
                  .addApi(Plus.API)
                  .addScope(Plus.SCOPE_PLUS_LOGIN).build();
          mGoogleApiClient.registerConnectionFailedListener(onConnectionFailedListener);
          mGoogleApiClient.registerConnectionCallbacks(connectionCallbacks);
      }
  }


    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            finish();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
  }

  protected void onStop(){
        super.onStop();
      if(mGoogleApiClient !=null && mGoogleApiClient.isConnected()){
          mGoogleApiClient.disconnect();
      }
    }

    @Override
    public void onClick(View v){
        signInWithPlus();
    }

    private void signInWithPlus(){
        if(!mGoogleApiClient.isConnecting()){
            mSignInClicked = true;
            resolveSignInError();
        }
    }

    private void resolveSignInError(){
        if (mConnectionResult != null && mConnectionResult.hasResolution()) {
            try {
                mResolvingError = true;
                mConnectionResult.startResolutionForResult(this, RC_SIGN_IN);
            } catch (IntentSender.SendIntentException e) {
                mGoogleApiClient.connect();
            }
        }
    }

    @Override
      public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode,resultCode,data);
        if(requestCode == RC_SIGN_IN)
        {
            mResolvingError = false;
            if (resultCode != RESULT_OK) {
                mSignInClicked = false;
            }
            // Make sure the app is not already connected or attempting to connect
            if (!mGoogleApiClient.isConnecting() &&
                    !mGoogleApiClient.isConnected()) {
                mGoogleApiClient.connect();
            }
        }
//        fbCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void getProfileInformation() {
        try {
            if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
                Person currentPerson = Plus.PeopleApi
                        .getCurrentPerson(mGoogleApiClient);
                String id = currentPerson.getId();
                String personName = currentPerson.getDisplayName();
                String personPhotoUrl = currentPerson.getImage().getUrl();
                String email = Plus.AccountApi.getAccountName(mGoogleApiClient);
                String birthday = currentPerson.getBirthday();
                int gender = currentPerson.getGender();
                SharedPreferences.Editor editor = pref.edit();
                editor.putString(Commons.USER_NAME, personName);
                editor.putString(Commons.EMAIL, email);
                editor.putString(Commons.PIC_URL, personPhotoUrl);
                if(gender == 0) {
                    editor.putString(Commons.GENDER, "Male");
                } else {
                    editor.putString(Commons.GENDER, "Female");
                }
                editor.commit();

            } else {
                Toast.makeText(getApplicationContext(),
                        "Person information is null", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Log.e("Error in Google SignIn", e.toString());
        }
    }
    GoogleApiClient.ConnectionCallbacks connectionCallbacks = new GoogleApiClient.ConnectionCallbacks() {
        @Override
        public void onConnected(Bundle bundle) {
            mSignInClicked = false;
            SharedPreferences.Editor editor = pref.edit();
            editor.putBoolean(Commons.HAS_USER_LOGGED_INTO_GOOGLE_PLUS, true);
            editor.commit();//HAS_USER_NEVER_LOGGED_IN
            getProfileInformation();

            mGoogleApiClient.unregisterConnectionCallbacks(connectionCallbacks);
            mGoogleApiClient.unregisterConnectionFailedListener(onConnectionFailedListener);

            Intent intent = new Intent(LoginActivity.this,HomeActivity.class);
            startActivity(intent);
            finish();
        }

        @Override
        public void onConnectionSuspended(int i) {
            mGoogleApiClient.connect();
        }
    };
    GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener = new GoogleApiClient.OnConnectionFailedListener() {
        @Override
        public void onConnectionFailed(ConnectionResult result) {
            //Log.e("login", "onConnectionFailed called" + result.toString());
            if (!result.hasResolution()) {
                GooglePlayServicesUtil.getErrorDialog(result.getErrorCode(), getParent(),
                        0).show();
                return;
            }

            if (!mResolvingError) {
                mConnectionResult = result;

                if (mSignInClicked) {
                    resolveSignInError();
                }
            }
        }
    };
}

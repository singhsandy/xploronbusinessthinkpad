package com.darkmatter.xploron.business;

import android.accounts.AccountManager;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.usage.UsageEvents;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.appspot.darkmatter_xploron.xploron.Xploron;
import com.appspot.darkmatter_xploron.xploron.model.Business;
import com.appspot.darkmatter_xploron.xploron.model.BusinessGetByOwner;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.json.gson.GsonFactory;
import com.google.gson.Gson;

import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by sandeep on 20-Nov-15.
 */
public class HomeActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener
        , BusinessAdapter.IMyViewHolderClicks {

    private FloatingActionButton addBusinessFAB;
    private SharedPreferences pref;
    private GoogleAccountCredential credential;
    private GoogleApiClient googleApiClient;
    private boolean doubleBackToExitPressedOnce = false;
    private SwipeRefreshLayout swipeRefreshLayout, swipeRefreshLayoutEmptyView;
    private String businessString;
    private List<BusinessGetByOwner> listOfBusiness = new ArrayList<>();
    private RecyclerView recyclerViewBusiness;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        recyclerViewBusiness = (RecyclerView) findViewById(R.id.recyclerViewBusiness);
        addBusinessFAB = (FloatingActionButton) findViewById(R.id.fabAddBusiness);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayoutEmptyView = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout_emptyView);
        addBusinessFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertCreateBusinessDialog();
            }
        });

        mLayoutManager = new LinearLayoutManager(this);
        recyclerViewBusiness.setLayoutManager(mLayoutManager);
        pref = PreferenceManager.getDefaultSharedPreferences(this);
        String emailId = pref.getString(Commons.EMAIL, "sandeepkumarsingh.1993@gmail.com");
        credential = GoogleAccountCredential.usingAudience(this,
                "server:client_id:454280588675-33ro5ieerek0mcfgkubrk7aqpfbivjge.apps.googleusercontent.com");
        credential.setSelectedAccountName(emailId);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayoutEmptyView.setOnRefreshListener(this);
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(true);
                new GetBusinessTask().execute();
            }
        });

        createClient();
    }

    protected void onStop() {
        super.onStop();
        if (googleApiClient.isConnected()) {
            googleApiClient.disconnect();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        SharedPreferences.Editor editor = pref.edit();
        if (id == R.id.action_logout) {
            if (googleApiClient.isConnected()) {
                Plus.AccountApi.clearDefaultAccount(googleApiClient);
//              googleApiClient.clearDefaultAccountAndReconnect();
                googleApiClient.disconnect();
            }
            editor.putString(Commons.USER_NAME, null);
            editor.putString(Commons.EMAIL, null);
            editor.putString(Commons.PIC_URL, null);
            editor.putString(Commons.GENDER, null);
            editor.putBoolean(Commons.HAS_USER_LOGGED_INTO_GOOGLE_PLUS, false);
            editor.commit();
            Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            finish();
            startActivity(intent);
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            finish();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        createClient();
        if (!googleApiClient.isConnecting() && !googleApiClient.isConnected()) {
            googleApiClient.connect();
        }
        setIntent(intent);
    }

    private void alertCreateBusinessDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(HomeActivity.this);
        dialog.setMessage("Please Create new Business at your office location only.");
        dialog.setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(HomeActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

        dialog.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void alertDialog(int position) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(HomeActivity.this);
        BusinessGetByOwner business = listOfBusiness.get(position);
        final String businessId = business.getBusinessId();
        Gson businessInJson = new Gson();
        businessString = businessInJson.toJson(business);
        dialog.setPositiveButton("Edit Offer", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent editOffer = new Intent(HomeActivity.this, OfferActivityForBusiness.class);
                editOffer.putExtra("BusinessId", businessId);
                startActivity(editOffer);
            }
        });

        dialog.setNegativeButton("Edit Business", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent editBusiness = new Intent(HomeActivity.this, MainActivity.class);
                editBusiness.putExtra("BusinessDescription", businessString);
                startActivity(editBusiness);
            }
        });
        dialog.show();
    }

    @Override
    public void alertLongPressDialog(int position) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(HomeActivity.this);
        BusinessGetByOwner business = listOfBusiness.get(position);
        final String businessId = business.getBusinessId();
        dialog.setMessage("Do You Want to Delete Business");
        dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                new RemoveBusiness(businessId).execute();
            }
        });

        dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void onRefresh() {
        new GetBusinessTask().execute();
    }

    private class RemoveBusiness extends AsyncTask<Void, Void, String> {
        String businessId;

        public RemoveBusiness(String businessId) {
            this.businessId = businessId;
        }

        @Override
        protected String doInBackground(Void... params) {
            String responseString;
            Xploron.Builder builder = new Xploron.Builder(AndroidHttp.newCompatibleTransport(),
                    new GsonFactory(), credential);
            builder.setRootUrl("https://darkmatter-xploron.appspot.com/_ah/api/");
            Xploron service = builder.build();
            try {
                service.deleteByBusinessId(businessId).execute();
                responseString = null;
            } catch (IOException ex) {
                Log.e("IOException", ex.getMessage());
                responseString = ex.getMessage();
            } catch (Exception ex) {
                Log.e("Exception BusinesDelete", ex.getMessage());
                responseString = ex.getMessage();
            }
            return responseString;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Intent intent = getIntent();
            if (result != null) {
                Toast.makeText(HomeActivity.this, "Couldn't Delete Business becauseof" + result + "", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(HomeActivity.this, "Business Deleted Successfully", Toast.LENGTH_SHORT).show();
            }
            finish();
            startActivity(intent);
        }
    }

    private void createClient() {
        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Plus.API).addScope(Plus.SCOPE_PLUS_LOGIN).build();

    }

    private class GetBusinessTask extends AsyncTask<Void, Void, List<BusinessGetByOwner>> {

        @Override
        protected List<BusinessGetByOwner> doInBackground(Void... param) {
            List<BusinessGetByOwner> businessesList;
            Xploron.Builder builder = new Xploron.Builder(AndroidHttp.newCompatibleTransport(), new GsonFactory(), credential);
            builder.setRootUrl("https://darkmatter-xploron.appspot.com/_ah/api/");
            Xploron service = builder.build();
            try {
//                if (service.getBusinessForOwner().execute() != null) {
                    businessesList = service.getBusinessForOwner().execute().getItems();
//                } else {
//                    businessesList = null;
//                }
            } catch (IOException ex) {
                businessesList = null;
                Log.e("IOException", ex.getMessage());
            } catch(Exception ex)
            {
                businessesList = null;
                Log.e("Exception",ex.getMessage());
            }
            return businessesList;
        }

        @Override
        protected void onPostExecute(List<BusinessGetByOwner> business) {
            if (!googleApiClient.isConnecting() && !googleApiClient.isConnected()) {
                googleApiClient.connect();
            }
            swipeRefreshLayout.setRefreshing(false);
            if (business != null) {
                listOfBusiness = business;
                BusinessAdapter adapter = new BusinessAdapter(HomeActivity.this,listOfBusiness);
                adapter.setCallback(HomeActivity.this);
                recyclerViewBusiness.setAdapter(adapter);
                swipeRefreshLayoutEmptyView.setVisibility(View.GONE);
            } else {
                swipeRefreshLayout.setVisibility(View.GONE);
                swipeRefreshLayoutEmptyView.setVisibility(View.VISIBLE);
            }
        }
    }
}

package com.darkmatter.xploron.business;

/**
 * Created by sandeep on 01-Dec-15.
 */
import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.util.Log;

/**
 * Created by DeathStar on 24/11/15.
 */
public class ConnectionUnavailableDialog extends DialogFragment {

    ConnectionDialogListener listener;
    String message;
    Context context;
    public ConnectionUnavailableDialog() {}

    public static ConnectionUnavailableDialog newInstance(String message) {
        ConnectionUnavailableDialog connectionUnavailableDialog = new ConnectionUnavailableDialog();
        Bundle bundle = new Bundle();
        bundle.putString("message", message);
        connectionUnavailableDialog.setArguments(bundle);
        return connectionUnavailableDialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        message = getArguments().getString("message");
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("No Internet connection")
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        listener.onPostiveClickListener(ConnectionUnavailableDialog.this);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        listener.onNegativeClickListener(ConnectionUnavailableDialog.this);
                    }
                });
        return builder.create();
    }

    public interface ConnectionDialogListener {
        public void onPostiveClickListener(DialogFragment dialog);
        public void onNegativeClickListener(DialogFragment dialog);
        public void onDialogDismissed(DialogFragment dialog);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            listener = (ConnectionDialogListener) activity;
        } catch (ClassCastException e) {
            Log.e("ConectnUnavailblDialog", e+"");
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        listener.onDialogDismissed(ConnectionUnavailableDialog.this);
    }
}
//package com.darkmatter.xploron.business;
//
//import android.content.Context;
//import android.database.sqlite.SQLiteDatabase;
//import android.database.sqlite.SQLiteOpenHelper;
//import android.util.Log;
//
//import org.apache.http.client.fluent.Content;
//
///**
// * Created by sandeep on 03-Dec-15.
// */
////public class DbHelper extends SQLiteOpenHelper {
////
////    private static final int DATABASE_VERSION = 1;
////    public static final String DATABASE_NAME = "bizztapbusiness.db";
////
////    public DbHelper(Context context){
////        super(context,DATABASE_NAME,null,DATABASE_VERSION);
////    }
////
////    @Override
////    public void onCreate(SQLiteDatabase db){
////        Log.e("","OnCreate of SQL called");
////
////        final String CREATE_LOCATION_TABLE =
////                "CREATE TABLE" + DbContract.Location.TABLE_NAME +
////                        "(" +
////                        DbContract.Location._ID +"INTEGER PRIMARY KEY, " +
////                        DbContract.Location.COL_CITY+ "TEXT, "+
////                        DbContract.Location.COL_LAT + "REAL, "+
////                        DbContract.Location.COL_LNG +" REAL, "+
////                        DbContract.Location.COL_LOCALITY + "TEXT, " +
////                        DbContract.Location.COL_STREET + "TEXT, "+
////                        DbContract.Location.COL_STATE +"TEXT, " +
////                        DbContract.Location.COL_PIN +"REAL, "+
////                        "UNIQUE (" + DbContract.Location.COL_LAT+ ","+DbContract.Location.COL_LNG+
////                        ") ON CONFLICT IGNORE);";
////
////        final String CREATE_BUSINESS_TABLE =
////                "CREATE_TABLE" + DbContract.Business.TABLE_NAME +
////                        "(" +
////                        DbContract.Business._ID+ "INTEGER PRIMARY KEY" +
////                        DbContract.Business.COLUMN_CATEGORY_KEY + "INTEGER NOT NULL, " +
////                        DbContract.Business.COLUMN_LOCATION_KEY + "INTEGER NOT NULL, " +
////                        DbContract.Business.COLUMN_OWNER_KEY + "INTEGER NOT NULL, " +
////                        DbContract.Business.COLUMN_CLOSE_HRS + "TEXT, " +
////                        DbContract.Business.COLUMN_OPEN_HRS + "TEXT, "+
////                        DbContract.Business.COLUMN_DAYS +"TEXT,"+
////                        DbContract.Business.COLUMN_DESC+"TEXT, "+
////                        DbContract.Business.COLUMN_EMAIL_ID+"TEXT,"+
////                        DbContract.Business.COLUMN_BUSINESS_NAME+"TEXT, "+
////                        DbContract.Business.COLUMN_WEBSITE +"TEXT, "+
////                        DbContract.Business.COLUMN_UPDATED_ON+ "TEXT, "+
////                        DbContract.Business.COLUMN_LANDLINES_ARR+"TEXT, "+
////                        DbContract.Business.COLUMN_MOBIES_ARR+"TEXT, " +
////                        DbContract.Business.COLUMN_VERIFIED+"INTEGER DEFAULT 0, "+
////
////    }
////}

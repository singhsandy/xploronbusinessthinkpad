package com.darkmatter.xploron.business;

import android.util.Log;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by RAHUL on 26-10-2015.
 */
public class BrandsUtil {



    public static ArrayList<String> getBrands(String category) {
        ArrayList<String> brands = new ArrayList<>();

            if(category.equalsIgnoreCase("Electronics")) {
                brands.add("Asus");brands.add("Acer");
                brands.add("Apple");brands.add("HP/Compaq");brands.add("Dell");
                brands.add("Lenovo");brands.add("Micromax");brands.add("Sony");
                brands.add("Intel");brands.add("Canon");brands.add("D-Link");
            } else

            if(category.equalsIgnoreCase("Health")) {
                brands.add("Reddy's"); brands.add("Johnson & Johnson"); brands.add("Amway");
                brands.add("Ranbaxy"); brands.add("Sun Pharma"); brands.add("Dabur");
            } else

            if(category.equalsIgnoreCase("Fashion") || category.equalsIgnoreCase("CLOTHES")) {
                brands.add("Levis"); brands.add("Lee"); brands.add("John Players");
                brands.add("Addidas"); brands.add("U.S. Polo"); brands.add("Wrangler");
            } else

            if(category.equalsIgnoreCase("FOOTWEAR") || category.equalsIgnoreCase("SHOES")) {
                brands.add("Addidas"); brands.add("Reebok"); brands.add("Woodland");
                brands.add("Fila"); brands.add("Asics"); brands.add("Puma");
            } else

            if(category.equalsIgnoreCase("BEAUTY PRODUCTS")) {
                brands.add("HIMANI"); brands.add("Lakme"); brands.add("L'oreal");
                brands.add("Revloon"); brands.add("Elle 18"); brands.add("Maybeline");
            }

        return brands;
    }
}

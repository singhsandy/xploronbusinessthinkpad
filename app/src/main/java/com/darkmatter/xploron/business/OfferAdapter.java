package com.darkmatter.xploron.business;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.appspot.darkmatter_xploron.xploron.model.Offers;
import com.bumptech.glide.Glide;

import org.w3c.dom.Text;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.FieldPosition;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by sandeep on 23-Nov-15.
 */
public class OfferAdapter extends RecyclerView.Adapter<OfferAdapter.ViewHolder> implements View.OnClickListener {
    public static final String TAG = "OfferAdapter";
    List<Offers> listOffers = new ArrayList<>();
    Context context;
    private IMyViewHolderClicks callback;
    public OfferAdapter(Context context,List<Offers> item){
        this.listOffers = item;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_offeritemlist,parent,false);
        return new ViewHolder(v,callback);
    }

    @Override
    public void onClick(View v) {

    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        TextView txtOfferName;
        TextView txtOfferPostedDate;
        TextView txtOfferValidTillDate;
        ImageView offerImage;
        public ViewHolder(View v, final IMyViewHolderClicks callback)
        {
            super(v);
                v.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(callback!=null)
                        {
                            callback.alertDialog(getPosition());
                        }
                    }
                });
                v.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        if(callback!=null)
                        {
                            callback.alertLongPressDialog(getPosition());
                        }
                        return true;
                    }
                });
            txtOfferName = (TextView)v.findViewById(R.id.txtOfferTitle);
            txtOfferPostedDate = (TextView)v.findViewById(R.id.txtOfferPostedOn);
            txtOfferValidTillDate = (TextView)v.findViewById(R.id.txtOfferValidTill);
            offerImage = (ImageView)v.findViewById(R.id.main_layout_offer_image);
        }
        public TextView getOfferNameTextView(){
            return txtOfferName;
        }
        public TextView getPostedDateTextView(){
            return txtOfferPostedDate;
        }
        public TextView getValidTillTextView(){
            return txtOfferValidTillDate;
        }
    }
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Offers offerItem = listOffers.get(position);
        String[] startDateArray = offerItem.getStartDate().toString().split("T");
        String[] validTillDateArray = offerItem.getValidTillDate().toString().split("T");
        String[] startDayMonthYearArray = startDateArray[0].split("-");
        String[] validDayMonthYearArray = validTillDateArray[0].split("-");
        String startDate = startDayMonthYearArray[2]+"-"+startDayMonthYearArray[1] +"-"+startDayMonthYearArray[0];
        String validDate = validDayMonthYearArray[2]+"-"+validDayMonthYearArray[1]+"-"+validDayMonthYearArray[0];
        List<String> urllist = offerItem.getListUrlsOfferImages();
        holder.getOfferNameTextView().setText(offerItem.getTitle());
        holder.getPostedDateTextView().setText(startDate);
        holder.getValidTillTextView().setText(validDate);
        if(urllist!=null) {
//            new setBitmapFromOfferUrl(holder, urllist.get(0)).execute();

            Glide.with(context).load(urllist.get(0)).into(holder.offerImage);
        }
    }
    @Override
    public int getItemCount() {
        return listOffers.size();
    }

//    private class setBitmapFromOfferUrl extends AsyncTask<Void,Void,Bitmap>
//    {
//        ViewHolder viewholder;
//        String urlstring;
//        public setBitmapFromOfferUrl(ViewHolder holder,String imageUrl){
//            this.viewholder = holder;
//            this.urlstring = imageUrl;
//        }
//        @Override
//        protected Bitmap doInBackground(Void...param){
//            Bitmap imageBitmap;
//            try {
//                URL url = new URL(urlstring);
//                HttpURLConnection connection = (HttpURLConnection)url.openConnection();
//                connection.setDoInput(true);
//                connection.connect();
//                InputStream urlInputStream = connection.getInputStream();
//                imageBitmap = BitmapFactory.decodeStream(urlInputStream);
//            }
//            catch (Exception ex)
//            {
//                imageBitmap = null;
//                Log.e("Exception", ex.getMessage());
//            }
//            return imageBitmap;
//        }
//        @Override
//        @SuppressWarnings("deprecation")
//        protected void onPostExecute(Bitmap imageBitmp){
//            viewholder.offerImage.setImageBitmap(imageBitmp);
//            super.onPostExecute(imageBitmp);
//        }
//    }
    public void setCallback(IMyViewHolderClicks callback){
            this.callback = callback;
    }

    public interface IMyViewHolderClicks {
        public void alertDialog(int position);
        public void alertLongPressDialog(int position);
    }
}

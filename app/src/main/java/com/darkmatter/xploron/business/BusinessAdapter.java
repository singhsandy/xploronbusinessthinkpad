package com.darkmatter.xploron.business;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.appspot.darkmatter_xploron.xploron.model.Business;
import com.appspot.darkmatter_xploron.xploron.model.BusinessGetByOwner;
import com.bumptech.glide.Glide;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

/**
 * Created by sandeep on 22-Nov-15.
 */

  public class BusinessAdapter extends RecyclerView.Adapter<BusinessAdapter.ViewHolder>{
    List<BusinessGetByOwner> listOfBusiness = new ArrayList<>();
    List<String> listOfUrl = new ArrayList<>();
    Context context;
    private IMyViewHolderClicks callback;
    public BusinessAdapter(Context context, List<BusinessGetByOwner> item){
        this.listOfBusiness = item;
        this.context = context;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtBusinessName;
        TextView txtBusinessAddress;
        ImageView imageView;
        public ViewHolder(View view, final IMyViewHolderClicks callback){
            super(view);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(callback!=null){
                        callback.alertDialog(getPosition());
                    }
                }
            });
            view.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if(callback!=null)
                        callback.alertLongPressDialog(getPosition());
                    return true;
                }
            });
            txtBusinessName = (TextView)view.findViewById(R.id.txtBusinessName);
            txtBusinessAddress=(TextView)view.findViewById(R.id.txtBusinessAddress);
            imageView = (ImageView) view.findViewById(R.id.main_layout_image);
        }
        public TextView getTxtBusinessName(){
            return txtBusinessName;
        }
        public TextView getTxtBusinessAddress(){
            return txtBusinessAddress;
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_businessitemlist,parent,false);
        return new ViewHolder(view,callback);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        BusinessGetByOwner businessItem = listOfBusiness.get(position);
        listOfUrl = businessItem.getListUrlImages();
        String businessAddress = businessItem.getStreet()+ ","+ businessItem.getLocality()
                +","+ businessItem.getCity() +","+businessItem.getState();
        holder.getTxtBusinessName().setText(businessItem.getBusinessName());
        holder.getTxtBusinessAddress().setText(businessAddress);
        if(listOfUrl!=null) {
//            holder.imageView.setImageDrawable(null);
//            new getBitmapFromUrl(holder, listOfUrl.get(0)).execute();
            Glide.with(context).load(listOfUrl.get(0)).into(holder.imageView);
       }
    }

    @Override
    public int getItemCount() {
        return listOfBusiness.size();
    }

//    private class getBitmapFromUrl extends AsyncTask<Void,Void,Bitmap>
//    {
//        ViewHolder viewholder;
//        String urlstring;
//        public getBitmapFromUrl(ViewHolder holder,String imageUrl){
//            this.viewholder = holder;
//            this.urlstring = imageUrl;
//        }
//
//        @Override
//        protected Bitmap doInBackground(Void...param){
//            Bitmap imageBitmap;
//            try {
//                URL url = new URL(urlstring);
//                HttpURLConnection connection = (HttpURLConnection)url.openConnection();
//                connection.setDoInput(true);
//                connection.connect();
//                InputStream urlInputStream = connection.getInputStream();
//                imageBitmap = BitmapFactory.decodeStream(urlInputStream);
//            }
//            catch (Exception ex)
//            {
//                imageBitmap = null;
//                Log.e("Exception", ex.getMessage());
//            }
//            return imageBitmap;
//        }
//
//        @Override
//        protected void onPostExecute(Bitmap imageBitmp){
//            viewholder.imageView.setImageBitmap(imageBitmp);
//            super.onPostExecute(imageBitmp);
//        }
//    }

    public void setCallback(IMyViewHolderClicks callback){
        this.callback = callback;
    }

    public interface IMyViewHolderClicks{
        public void alertDialog(int position);
        public void alertLongPressDialog(int position);
    }

}

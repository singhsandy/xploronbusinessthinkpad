package com.darkmatter.xploron.business;

import android.app.DialogFragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.support.v7.app.ActionBarActivity;

import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import org.json.JSONObject;


/**
 * Created by sandeep on 01-Dec-15.
 */
public class SplashActivity extends AppCompatActivity
        implements ConnectionUnavailableDialog.ConnectionDialogListener {

    private static final long SPLASH_TIME_OUT = 2000;
    private AccessTokenTracker accessTokenTracker;
    private ProfileTracker profileTracker;

    private SharedPreferences prefs;
    private boolean isUserLoggedIntoGoogle = false;
//    private boolean neverLoggedIn;
    TextView splashText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
        splashText = (TextView) findViewById(R.id.splash_text);
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        isUserLoggedIntoGoogle = prefs.getBoolean(Commons.HAS_USER_LOGGED_INTO_GOOGLE_PLUS, false);
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Light.ttf");
        splashText.setTypeface(face);
        if(!Commons.isConnectingToInternet(this)) {
            // we do not have connection
            DialogFragment dialogFragment;
                dialogFragment = ConnectionUnavailableDialog
                        .newInstance("If you cancel application will close");

            dialogFragment.show(getFragmentManager(), "no_internet");
        } else {
        if (isUserLoggedIntoGoogle) {
            //Log.e("Splash", "user logged into Google plus");
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
//                    Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
            }, SPLASH_TIME_OUT);
        } else {
            FacebookSdk.sdkInitialize(getApplicationContext());

            accessTokenTracker = new AccessTokenTracker() {
                @Override
                protected void onCurrentAccessTokenChanged(AccessToken oldToken, AccessToken newToken) {
                    //Log.e("Facebook splash", "Current Access Token changed");
                    updateWithToken(newToken);
                }
            };

            profileTracker = new ProfileTracker() {
                @Override
                protected void onCurrentProfileChanged(Profile oldProfile, Profile newProfile) {
                    //Log.e("Facebook splash", "Current Profile changed");
                }
            };

            accessTokenTracker.startTracking();
            profileTracker.startTracking();
            updateWithToken(AccessToken.getCurrentAccessToken());
        }
    }
    }

    private void updateWithToken(final AccessToken currentAccessToken) {

        if (currentAccessToken != null) {
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    GraphRequest request = GraphRequest.newMeRequest(
                            AccessToken.getCurrentAccessToken(),
                            new GraphRequest.GraphJSONObjectCallback() {
                                @Override
                                public void onCompleted(JSONObject object, GraphResponse response) {
                                    // Application code
                                    Commons.parseFacebookGraphAPIresponse(response, getApplicationContext());
                                    //Log.e("Graph API response", response.toString());
                                }
                            });
                    Bundle parameters = new Bundle();
                    parameters.putString("fields", "id,name,email,gender,picture,birthday");
                    request.setParameters(parameters);
                    request.executeAsync();
//                    Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                    Intent i = new Intent(SplashActivity.this, HomeActivity.class);
                    startActivity(i);
                    finish();
                }
            }, SPLASH_TIME_OUT);
        } else {
            //Log.e("updateWithToken splash", "current access token is null");
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    //Log.e("updateWithToken splash", "inside run method");
                    Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }
            }, SPLASH_TIME_OUT);
        }
    }
    @Override
    public void onPostiveClickListener(DialogFragment dialog) {
        Intent intent = new Intent(Settings.ACTION_SETTINGS);
        startActivity(intent);
        finish();
    }

    @Override
    public void onNegativeClickListener(DialogFragment dialog) {
        finish();
    }

    @Override
    public void onDialogDismissed(DialogFragment dialog) {
       finish();
    }
}


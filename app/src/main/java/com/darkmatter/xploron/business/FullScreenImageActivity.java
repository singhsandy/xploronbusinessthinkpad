package com.darkmatter.xploron.business;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;


/**
 * Created by sandeep on 01-Dec-15.
 */
public class FullScreenImageActivity extends ActionBarActivity {

    String imageUrl;
    ImageView imageView;
    ImageButton deleteImageBtn;
    int position;

    @Override
    protected void onCreate(Bundle onSavedInstanceState) {
        super.onCreate(onSavedInstanceState);
        setContentView(R.layout.activity_fullscreen_image);
        imageView = (ImageView) findViewById(R.id.fullScreenImage);
        deleteImageBtn = (ImageButton)findViewById(R.id.delete_imagebtn);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            imageUrl = bundle.getString("ImageUrl");
            position = bundle.getInt("Position");
        }

        deleteImageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertImageDelete();
            }
        });
        new urltoImage().execute(imageUrl);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
            int id = item.getItemId();
            if(id==android.R.id.home) {
//                NavUtils.navigateUpFromSameTask(this);
                finish();
                return true;
            }
                return super.onOptionsItemSelected(item);
        }
    private void alertImageDelete(){
        AlertDialog.Builder dialog = new AlertDialog.Builder(FullScreenImageActivity.this);
        dialog.setMessage("Are You sure to delete this image.");
        dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent();
                intent.putExtra("Position",position);
                setResult(Activity.RESULT_OK,intent);
                finish();
            }
        });
        dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    private class urltoImage extends AsyncTask<String, Void, Bitmap> {

        @Override
        protected Bitmap doInBackground(String... params) {
            String urlString = params[0];
            Bitmap imageBitmap;
            try {
                URL url = new URL(urlString);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream urlInputStream = connection.getInputStream();
                imageBitmap = BitmapFactory.decodeStream(urlInputStream);
            } catch (Exception ex) {
                imageBitmap = null;
                Log.e("Exception", ex.getMessage());
            }
            return imageBitmap;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            imageView.setImageBitmap(result);
        }
    }
}

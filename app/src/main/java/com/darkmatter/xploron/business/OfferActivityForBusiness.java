package com.darkmatter.xploron.business;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.NavUtils;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.appspot.darkmatter_xploron.xploron.Xploron;
import com.appspot.darkmatter_xploron.xploron.model.Offers;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.json.gson.GsonFactory;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sandeep on 23-Nov-15.
 */
public class OfferActivityForBusiness extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener
        , OfferAdapter.IMyViewHolderClicks {

    private RecyclerView recyclerViewOffers;
    private FloatingActionButton fabCreateOffer;
    private SharedPreferences prefs;
    private SwipeRefreshLayout swipeRefreshLayout, swipeRefreshLayoutEmptyView;
    GoogleAccountCredential credentials;
    private String businessId, offerString;
    protected RecyclerView.LayoutManager mLayoutManager;
    private TextView emptyText;
    public static final String PREF_ACCOUNT_NAME = "PREF_ACCOUNT_NAME";
    List<Offers> offerList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offerforbusiness);
        recyclerViewOffers = (RecyclerView) findViewById(R.id.recyclerViewOffers);
        mLayoutManager = new LinearLayoutManager(this);
        recyclerViewOffers.setLayoutManager(mLayoutManager);
        emptyText = (TextView) findViewById(android.R.id.empty);
        fabCreateOffer = (FloatingActionButton) findViewById(R.id.fabCreateOffers);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_Offerlayout);
        swipeRefreshLayoutEmptyView = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout_emptyView);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Bundle businessBundle = getIntent().getExtras();
        if (businessBundle != null) {
            businessId = businessBundle.getString("BusinessId");
        }
        fabCreateOffer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OfferActivityForBusiness.this, OfferActivity.class);
                intent.putExtra("BusinessId", businessId);
                startActivity(intent);
            }
        });
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String email = prefs.getString(Commons.EMAIL, "sandeepkumarsingh.1993@gmail.com");
        credentials = GoogleAccountCredential.usingAudience(this,
                "server:client_id:454280588675-33ro5ieerek0mcfgkubrk7aqpfbivjge.apps.googleusercontent.com");
        credentials.setSelectedAccountName(email);

        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayoutEmptyView.setOnRefreshListener(this);
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(true);
                new GetOfferTask().execute();
            }
        });
//        swipeRefreshLayoutEmptyView.post(new Runnable() {
//            @Override
//            public void run() {
//                swipeRefreshLayoutEmptyView.setRefreshing(true);
//                new GetOfferTask().execute();
//            }
//        });
    }

    @Override
    public void alertDialog(int position) {
        Log.e("OfferActivity Position", position + "");
        AlertDialog.Builder dialog = new AlertDialog.Builder(OfferActivityForBusiness.this);
        Offers offer = offerList.get(position);
        Gson offerInJson = new Gson();
        offerString = offerInJson.toJson(offer);
        dialog.setMessage("Do you want to edit Offer");
        dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(OfferActivityForBusiness.this, OfferActivity.class);
                intent.putExtra("OfferDetails", offerString);
                intent.putExtra("BusinessId", businessId);
                startActivity(intent);
            }
        });
        dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void alertLongPressDialog(int position) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(OfferActivityForBusiness.this);
        Offers offer = offerList.get(position);
        final Long offerId = offer.getOfferId();
        dialog.setMessage("Do You Want to Delete this Offer.");
        dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                new RemoveOffer(businessId, offerId).execute();
            }
        });
        dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            NavUtils.navigateUpFromSameTask(this);
        }
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onRefresh() {
        new GetOfferTask().execute();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    private class RemoveOffer extends AsyncTask<Void, Void, String> {
        String businessId;
        Long offerId;
        String statusReport;

        public RemoveOffer(String businessId, Long offerId) {
            this.businessId = businessId;
            this.offerId = offerId;
        }

        @Override
        protected String doInBackground(Void... param) {
            Xploron.Builder builder = new Xploron.Builder(AndroidHttp.newCompatibleTransport(),
                    new GsonFactory(), credentials);
            builder.setRootUrl("https://darkmatter-xploron.appspot.com/_ah/api");
            Xploron service = builder.build();
            try {
                service.deleteOffer(businessId, offerId).execute();
                statusReport = null;
            } catch (IOException ex) {
                Log.e("IOException RemoveOffer", ex.getMessage());
                statusReport = ex.getMessage();
            } catch (Exception ex) {
                Log.e("Exception RemoveOffer", ex.getMessage());
                statusReport = ex.getMessage();
            }
            return statusReport;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Intent intent = getIntent();
            finish();
            startActivity(intent);
            if (result == null) {
                Toast.makeText(OfferActivityForBusiness.this, "Offer Deleted Successfully", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(OfferActivityForBusiness.this, "Couldn't Delete Offer becauseof" + result + " ", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class GetOfferTask extends AsyncTask<Void, Void, List<Offers>> {

        @Override
        protected List<Offers> doInBackground(Void... params) {
            List<Offers> listOffer;
            Xploron.Builder builder = new Xploron.Builder(AndroidHttp.newCompatibleTransport(),
                    new GsonFactory(), credentials);
            builder.setRootUrl("https://darkmatter-xploron.appspot.com/_ah/api/");
            Xploron service = builder.build();
            try {
//                if (service.getOffersForOwner(businessId).execute() != null) {
                    listOffer = service.getOffersForOwner(businessId).execute().getItems();
//                } else
//                    listOffer = null;
            } catch (IOException ex) {
                Log.e("IOException", ex.getMessage());
                listOffer = null;
            } catch(Exception ex)
            {
                Log.e("Exception",ex.getMessage());
                listOffer = null;
            }
            return listOffer;
        }

        @Override
        protected void onPostExecute(List<Offers> offers) {
            swipeRefreshLayout.setRefreshing(false);
            if (offers != null) {
                offerList = offers;
                OfferAdapter adapter = new OfferAdapter(OfferActivityForBusiness.this,offerList);
                adapter.setCallback(OfferActivityForBusiness.this);
                recyclerViewOffers.setAdapter(adapter);
                swipeRefreshLayoutEmptyView.setVisibility(View.GONE);
            } else {
                swipeRefreshLayout.setVisibility(View.GONE);
                swipeRefreshLayoutEmptyView.setVisibility(View.VISIBLE);
            }
            super.onPostExecute(offers);
        }
    }
}

package com.darkmatter.xploron.business;

import android.accounts.AccountManager;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.appspot.darkmatter_xploron.xploron.Xploron;
import com.appspot.darkmatter_xploron.xploron.model.Offers;
import com.appspot.darkmatter_xploron.xploron.model.OffersForm;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.json.gson.GsonFactory;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.TimeZone;

// title, desc, validTill Date, posted date

public class OfferActivity extends AppCompatActivity {

    GoogleAccountCredential credentials;
    SharedPreferences prefs;
    String accountName;
    EditText offerTitleText;
    EditText offerDescText;
    static TextView validUptoText, startFromText;
    Button createOffer;
    String offertTitle, offerDesc, validUpto, businessId, startFrom;
    private Uri fileuri, selectedImage;
    LinearLayout selectedPhotoContainer;
    HorizontalScrollView horizontalScrollView;
    static private String fileName;
    String imgDecodableString, offerDetails;
    Long offerId;
    int imageCount = 0;
    long totalSize = 0;
    private ProgressDialog progressDialog;
    boolean isImageUploadedthroughflash = false;
    boolean isImageFitToScreen;
    private static final String TAG = OfferActivity.class.getSimpleName();
    List<String> listServingOfferImageUrl = new ArrayList<String>();
    List<String> listOfferImageBlobKey = new ArrayList<>();
    List<String> listDeletedOfferImageUrl = new ArrayList<String>();
    List<String> listDeletedOfferBlobKey = new ArrayList<>();
    List<ImageView> imageLayoutList = new ArrayList<>();
    List<ImageButton> cancelOfferImageList = new ArrayList<>();
    Bitmap bitmap;
    Display display;
    private Bundle bundleFromOffers;
    ImageButton btnCaptureOfferPicture, btnCaptureOfferPictureGallery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offer);

        offerDescText = (EditText) findViewById(R.id.offer_desc);
        offerTitleText = (EditText) findViewById(R.id.offer_title);
        validUptoText = (TextView) findViewById(R.id.valid_upto);
        startFromText = (TextView) findViewById(R.id.start_from);
        createOffer = (Button) findViewById(R.id.create_offer);
        btnCaptureOfferPicture = (ImageButton) findViewById(R.id.btncameraUpload);
        btnCaptureOfferPictureGallery = (ImageButton) findViewById(R.id.btngalleryUpload);
        selectedPhotoContainer = (LinearLayout) findViewById(R.id.selected_photos_container);
        horizontalScrollView = (HorizontalScrollView) findViewById(R.id.hori_scroll_view);
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String email = prefs.getString(Commons.EMAIL, "sandeepkumarsingh.1993@gmail.com");
//        String emailOwner = prefs.getString(Commons.EMAIL_OWNER, "sandeepkumarsingh.1993@gmail.com");
        credentials = GoogleAccountCredential.usingAudience(this,
                "server:client_id:454280588675-33ro5ieerek0mcfgkubrk7aqpfbivjge.apps.googleusercontent.com");
        credentials.setSelectedAccountName(email);
        display = getWindowManager().getDefaultDisplay();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        bundleFromOffers = getIntent().getExtras();
        businessId = bundleFromOffers.getString("BusinessId");
        offerDetails = bundleFromOffers.getString("OfferDetails");
        if (offerDetails != null) {
            createOffer.setText("Update Offer");
            setTitle("Update Offer");

            try {
                JSONObject jsonObject = new JSONObject(offerDetails);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
                Long timeStampValidDate = Long.parseLong(jsonObject.getJSONObject("validTillDate").getString("value"));
                Date dateValidTill = new Date(timeStampValidDate);
                String validDateStr = simpleDateFormat.format(dateValidTill);
                Long timeStampdateStartFrom = Long.parseLong(jsonObject.getJSONObject("startDate").getString("value"));
                Date dateStartFrom = new Date(timeStampdateStartFrom);
                String startDateStr = simpleDateFormat.format(dateStartFrom);
                offerId = jsonObject.getLong("offerId");
                offerTitleText.setText(jsonObject.getString("title"));
                offerDescText.setText(jsonObject.getString("description"));
                validUptoText.setText(validDateStr);
                startFromText.setText(startDateStr);
                if (jsonObject.has("listUrlsOfferImages")) {
                    JSONArray jsonArray = jsonObject.getJSONArray("listUrlsOfferImages");
                    JSONArray jsonBlobKeyArray = jsonObject.getJSONArray("listBlobKeysOffer");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        listServingOfferImageUrl.add(jsonArray.getString(i));
                        listOfferImageBlobKey.add(jsonBlobKeyArray.getString(i));
                        new urlToBitmapImage().execute(jsonArray.getString(i));
                    }
                }
            } catch (Exception ex) {
                Log.e("Exception", ex.getMessage());
            }
        } else {
            createOffer.setText("Create Offer");
            setTitle("Create Offer");
        }
        validUptoText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new ValidDatePickerFragment();
                newFragment.show(getSupportFragmentManager(), "validuptodatePicker");
            }
        });

        startFromText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new StartDatePickerFragment();
                newFragment.show(getSupportFragmentManager(), "startfromdatePicker");
            }
        });
        createOffer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                offertTitle = offerTitleText.getText().toString();
                offerDesc = offerDescText.getText().toString();
                validUpto = validUptoText.getText().toString();
                startFrom = startFromText.getText().toString();

                boolean fieldsOK = validate(new EditText[]{offerTitleText, offerDescText});
                removeDeletedImageUrlsFromServingUrl();
                if (fieldsOK) {
                    new MakePostOfferTask(offerId, businessId).execute();
                } else {
                    Toast.makeText(OfferActivity.this, "Please fill all edit box", Toast.LENGTH_SHORT).show();
                    return;
                }
            }
        });
        btnCaptureOfferPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                captureImage();
            }
        });

        btnCaptureOfferPictureGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                captureImageThroughGallery();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_profile) {
            startActivity(new Intent(this, MainActivity.class));
            return true;
        }
        if (id == android.R.id.home) {
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public static class ValidDatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            month++;
            String date = String.valueOf(day);
            String Month = String.valueOf(month);
            if (month < 10) {

                Month = "0" + month;
            }
            if (day < 10) {

                date = "0" + day;
            }
            validUptoText.setText(date + "-" + Month + "-" + year);
        }
    }

    public static class StartDatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            month++;
            String date = String.valueOf(day);
            String Month = String.valueOf(month);
            if (month < 10) {

                Month = "0" + month;
            }
            if (day < 10) {

                date = "0" + day;
            }
            startFromText.setText(date + "-" + Month + "-" + year);
        }
    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Commons.CAMERA_CAPTURE_IMAGE_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    isImageUploadedthroughflash = true;
                    previewMedia(true);
                } else if (resultCode == RESULT_CANCELED) {
                    Toast.makeText(getApplicationContext(),
                            "User cancelled image capture", Toast.LENGTH_SHORT)
                            .show();
                } else {
                    // failed to capture image
                    Toast.makeText(getApplicationContext(),
                            "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                            .show();
                }
                break;

            case Commons.GALLERY_CAPTURE_IMAGE_REQUEST_CODE:
                if (resultCode == RESULT_OK && data != null) {
                    selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    // Get the cursor
                    Cursor cursor = getContentResolver().query(selectedImage,
                            filePathColumn, null, null, null);
                    // Move to first row
                    cursor.moveToFirst();
                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    imgDecodableString = cursor.getString(columnIndex);
                    cursor.close();
                    isImageUploadedthroughflash = false;
                    previewMedia(true);
                } else {
                    Toast.makeText(this, "You haven't picked Image",
                            Toast.LENGTH_LONG).show();
                }
                break;
            case Commons.REQUEST_CODE_DELETE_IMAGE:
                if (resultCode == RESULT_OK) {
                    int selected_index = data.getIntExtra("Position", 0);
                    imageLayoutList.get(selected_index).setVisibility(View.GONE);
                    listDeletedOfferImageUrl.add(listServingOfferImageUrl.get(selected_index));
                    listDeletedOfferBlobKey.add(listOfferImageBlobKey.get(selected_index));
                } else {
                    Toast.makeText(getApplicationContext(), "Sorry Couldn't delete Image", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private boolean validate(EditText[] fields) {
        for (int i = 0; i < fields.length; i++) {
            EditText currentField = fields[i];
            if (currentField.getText().toString().length() <= 0) {
                return false;
            }
        }

        return true;
    }

    private void removeDeletedImageUrlsFromServingUrl() {
        for (Iterator<String> iterator = listDeletedOfferImageUrl.iterator(); iterator.hasNext(); ) {
            String imageurl = iterator.next();
            listServingOfferImageUrl.remove(imageurl);
        }
        for (Iterator<String> iterator = listDeletedOfferBlobKey.iterator(); iterator.hasNext(); ) {
            String blobUrl = iterator.next();
            listOfferImageBlobKey.remove(blobUrl);
        }
        Log.e("Length of serving URLs", String.valueOf(listServingOfferImageUrl.size()));
    }


    private void captureImage() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        fileuri = getOutputMediaFileUri(Commons.MEDIA_TYPE_IMAGE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileuri);

        startActivityForResult(intent, Commons.CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    private static File getOutputMediaFile(int type) {
        File mediaStorageDir = new File(
                Environment.getExternalStoragePublicDirectory
                        (Environment.DIRECTORY_PICTURES), Commons.IMAGE_DIRECTORY_NAME);
        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(TAG, "Oops! Failed create "
                        + Commons.IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == Commons.MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
            fileName = new String("IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }
        return mediaFile;
    }

    private void captureImageThroughGallery() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, Commons.GALLERY_CAPTURE_IMAGE_REQUEST_CODE);
    }

    private void previewMedia(boolean isImage) {
        // Checking whether captured media is image or video
        if (isImage) {
            LinearLayout.LayoutParams paramsImagePreview = new LinearLayout.LayoutParams(300, 300);
            horizontalScrollView.setVisibility(View.VISIBLE);
            paramsImagePreview.setMargins(0, 0, 20, 0);
            ImageView imagePreview = new ImageView(this);
            imagePreview.setLayoutParams(paramsImagePreview);
            imagePreview.setScaleType(ImageView.ScaleType.FIT_XY);
            imagePreview.setTag(imageCount);
            imageCount++;
            imagePreview.setOnClickListener(imagePreviewClickListener);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 8;
            String pathName;
            if (isImageUploadedthroughflash) {
                pathName = fileuri.getPath();
            } else {
                pathName = imgDecodableString;
            }
            bitmap = BitmapFactory.decodeFile(pathName, options);
            imagePreview.setImageBitmap(bitmap);
            imageLayoutList.add(imagePreview);
            selectedPhotoContainer.addView(imagePreview);
            progressDialog = ProgressDialog.show(this, "", "Loading...");
            new UploadOfferFileToServer().execute();
        }
    }

    private View.OnClickListener imagePreviewClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int selected_index = (Integer) v.getTag();
            String servingUrl = listServingOfferImageUrl.get(selected_index);
            Intent intent = new Intent(OfferActivity.this, FullScreenImageActivity.class);
            intent.putExtra("ImageUrl", servingUrl);
            intent.putExtra("Position", selected_index);
            startActivityForResult(intent, Commons.REQUEST_CODE_DELETE_IMAGE);
        }
    };

    private class urlToBitmapImage extends AsyncTask<String, Void, Bitmap> {
        @Override
        protected Bitmap doInBackground(String... param) {
            String imageUrl = param[0];
            InputStream urlInputStream;
            Bitmap bitmapimage;
            try {
                URL url = new URL(imageUrl);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                urlInputStream = connection.getInputStream();
                bitmapimage = BitmapFactory.decodeStream(urlInputStream);
            } catch (Exception ex) {
                bitmapimage = null;
                Log.e("Exception", ex.getMessage());
            }
            Log.e("urlToImage", bitmapimage + "");
            return bitmapimage;
        }

        @Override
        protected void onPostExecute(Bitmap bitmapresult) {
            ImageView imagePreview = new ImageView(OfferActivity.this);
            LinearLayout.LayoutParams imageParam = new LinearLayout.LayoutParams(300, 300);
            horizontalScrollView.setVisibility(View.VISIBLE);
            imageParam.setMargins(0, 0, 20, 0);
            imagePreview.setTag(imageCount);
            imageCount++;
            imagePreview.setOnClickListener(imagePreviewClickListener);
            bitmap = bitmapresult;
            imagePreview.setScaleType(ImageView.ScaleType.FIT_XY);
            imagePreview.setImageBitmap(bitmap);
            imagePreview.setLayoutParams(imageParam);
            imageLayoutList.add(imagePreview);
            selectedPhotoContainer.addView(imagePreview);
            Log.e("FinalInputStream", bitmap + "");
            super.onPostExecute(bitmapresult);
        }
    }

    private class MakePostOfferTask extends AsyncTask<Void, Void, Offers> {
        Long offerIdParam;
        String businessIdParam;

        public MakePostOfferTask(Long offerId, String businessId) {
            this.offerIdParam = offerId;
            this.businessIdParam = businessId;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(OfferActivity.this);
            if (offerIdParam != null) {
                progressDialog.setMessage("Updating Offer...");
            } else {
                progressDialog.setMessage("Creating New Offer...");
            }
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Offers doInBackground(Void... params) {
            Xploron.Builder builder = new Xploron.Builder(
                    AndroidHttp.newCompatibleTransport(), new GsonFactory(),
                    credentials);
            builder.setRootUrl("https://darkmatter-xploron.appspot.com/_ah/api/");
            Xploron service = builder.build();
            OffersForm offersForm = new OffersForm();
            offersForm.setBusinessId(businessIdParam);
            offersForm.setTitle(offertTitle);
            offersForm.setDescription(offerDesc);
            offersForm.setValidTillDate(validUpto);
            offersForm.setStartDate(startFrom);
            offersForm.setListBlobKeysOffer(listOfferImageBlobKey);
            offersForm.setListUrlsOfferImages(listServingOfferImageUrl);
            try {
                if (listDeletedOfferBlobKey.size() > 0) {
                    service.removeImages(listDeletedOfferBlobKey).execute();
                }
                Offers response;
                if (offerIdParam == null) {
                    response = service.saveOffers(offersForm).execute();
                } else {
                    response = service.updateOffers(offerIdParam, offersForm).execute();
                }
                return response;
            } catch (IOException e) {
                Log.e("IOException", e.getMessage() + "");
                return null;
            } catch (Exception e) {
                Log.e("Exception", e.getMessage() + "");
                return null;
            }
        }

        @Override
        protected void onPostExecute(Offers offerResult) {
            if (offerResult != null) {
                progressDialog.dismiss();
                if (offerIdParam != null) {
                    Toast.makeText(getApplicationContext(), "You have Updated Offer For Your Business.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Congratulation You have Created Offer for Your Business.", Toast.LENGTH_SHORT).show();
                }
                Intent intent = new Intent(OfferActivity.this, OfferActivityForBusiness.class);
                intent.putExtra("BusinessId", businessIdParam);
                startActivity(intent);
            } else {
                Toast.makeText(getApplicationContext(), "There is some error in creating offer.", Toast.LENGTH_SHORT);
            }
            finish();
            super.onPostExecute(offerResult);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            NavUtils.navigateUpFromSameTask(this);
        }
        return false;
    }

    private class UploadOfferFileToServer extends AsyncTask<Void, Integer, String> {
        @Override
        protected String doInBackground(Void... params) {
            return uploadFile();
        }

        @SuppressWarnings("deprecation")
        private String uploadFile() {

//            AndroidMultiPartEntity entity = new AndroidMultiPartEntity(HttpMultipartMode.BROWSER_COMPATIBLE,
//                    new AndroidMultiPartEntity.ProgressListener() {
//
//                        @Override
//                        public void transferred(long num) {
//                            Log.e("NUMINPROGRESSLISTNER", num + " " + totalSize);
//                            publishProgress((int) ((num * 100 / (float) totalSize)));
//                        }
//                    });
            MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            HttpClient httpClient = new DefaultHttpClient();
            HttpGet httpGet_new = new HttpGet("https://darkmatter-xploron.appspot.com/bloburlget"); // 10.0.2.2 is localhost's IP address in Android emulator
            try {

                HttpResponse response = httpClient.execute(httpGet_new);
                HttpEntity urlEntity = response.getEntity();
                InputStream in = urlEntity.getContent();
                String str = "";
                while (true) {
                    int ch = in.read();
                    if (ch == -1)
                        break;
                    str += (char) ch;
                }
                Log.e("biztap upload url: ", str);
                HttpPost httppost = new HttpPost(str);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                Bitmap bitmap;
                String filePathName;
                if (isImageUploadedthroughflash) {
                    bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), fileuri);
                    filePathName = fileName;
                    Log.e("Biztap: Image Uri is: ", fileuri + "");

                } else {
                    bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImage);
                    filePathName = imgDecodableString;
                }
                bitmap.compress(Bitmap.CompressFormat.JPEG, 50, baos);
                byte[] imageBytes = baos.toByteArray();

                ByteArrayBody bab = new ByteArrayBody(imageBytes, filePathName);
                entity.addPart("photo", bab);
                httppost.setEntity(entity);
                response = httpClient.execute(httppost); //Here "uploaded" servlet is automatically       invoked
                urlEntity = response.getEntity(); //Response will be returned by "uploaded" servlet in JSON format
                in = urlEntity.getContent();
                str = "";
                while (true) {
                    int ch = in.read();
                    if (ch == -1)
                        break;
                    str += (char) ch;
                }

                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 200) {
                    // Server response
                    Log.e("biztap uploaded url: ", str);
                    progressDialog.dismiss();
                } else {
                    str = "Error occurred! Http Status Code: "
                            + statusCode;
                    progressDialog.dismiss();
                }
//                return "Uploaded URL generated: " + str + "";
                return str;
            } catch (ClientProtocolException e) {
                return e.getMessage();
            } catch (IOException e) {
                return e.getMessage();
            }
        }

        @Override
        protected void onPostExecute(String result) {
            try {
                JSONObject obj = new JSONObject(result);
                String servingUrl = obj.getString("servingUrl");
                String servingBlobKey = obj.getString("blobKey");
                listServingOfferImageUrl.add(servingUrl);
                listOfferImageBlobKey.add(servingBlobKey);
            } catch (JSONException e) {
                Log.e("Json Exception", "Could not convert string to json");
            }
            super.onPostExecute(result);
        }
    }
}

[1mdiff --git a/app/libs/xploron-v1-1.21.0-SNAPSHOT.jar b/app/libs/xploron-v1-1.21.0-SNAPSHOT.jar[m
[1mindex 58b93fd..561ed39 100644[m
Binary files a/app/libs/xploron-v1-1.21.0-SNAPSHOT.jar and b/app/libs/xploron-v1-1.21.0-SNAPSHOT.jar differ
[1mdiff --git a/app/src/main/AndroidManifest.xml b/app/src/main/AndroidManifest.xml[m
[1mindex 599160c..7eef791 100644[m
[1m--- a/app/src/main/AndroidManifest.xml[m
[1m+++ b/app/src/main/AndroidManifest.xml[m
[36m@@ -5,7 +5,6 @@[m
     <uses-permission android:name="android.permission.INTERNET" />[m
     <uses-permission android:name="android.permission.GET_ACCOUNTS" />[m
     <uses-permission android:name="android.permission.USE_CREDENTIALS" />[m
[31m-    <uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION" />[m
     <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />[m
     <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />[m
 [m
[1mdiff --git a/app/src/main/java/com/darkmatter/xploron/business/Commons.java b/app/src/main/java/com/darkmatter/xploron/business/Commons.java[m
[1mindex accd69d..8541d21 100644[m
[1m--- a/app/src/main/java/com/darkmatter/xploron/business/Commons.java[m
[1m+++ b/app/src/main/java/com/darkmatter/xploron/business/Commons.java[m
[36m@@ -47,10 +47,9 @@[m [mpublic class Commons {[m
     public static final Pattern VALID_EMAIL_ADDRESS_REGEX =[m
             Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);[m
 [m
[31m-    public static String[] categories = {"Electronics", "Health", "Fashion", "Shoes",[m
[32m+[m[32m    public static String[] categories = {"Electronics", "Health", "Fashion", "Footwear",[m
             "Restaurant", "Medicine", "Sports Equipment",[m
[31m-            "Grocery", "Gift Shops", "Automobiles / Repair", "Saloon", "Beauty Parlor",[m
[31m-            "Beauty Products"};[m
[32m+[m[32m            "Grocery", "Gift Shops", "Automobiles / Repair", "Salon", "Kidswear", "Spa"};[m
 [m
 [m
     public static boolean isConnectingToInternet(Context context) {[m
[1mdiff --git a/app/src/main/java/com/darkmatter/xploron/business/HomeActivity.java b/app/src/main/java/com/darkmatter/xploron/business/HomeActivity.java[m
[1mindex 470eb87..d24064e 100644[m
[1m--- a/app/src/main/java/com/darkmatter/xploron/business/HomeActivity.java[m
[1m+++ b/app/src/main/java/com/darkmatter/xploron/business/HomeActivity.java[m
[36m@@ -53,26 +53,26 @@[m [mimport java.util.List;[m
  * Created by sandeep on 20-Nov-15.[m
  */[m
 public class HomeActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener[m
[31m-                            ,BusinessAdapter.IMyViewHolderClicks{[m
[32m+[m[32m        , BusinessAdapter.IMyViewHolderClicks {[m
 [m
     private FloatingActionButton addBusinessFAB;[m
     private SharedPreferences pref;[m
     private GoogleAccountCredential credential;[m
     private GoogleApiClient googleApiClient;[m
     private boolean doubleBackToExitPressedOnce = false;[m
[31m-    private SwipeRefreshLayout swipeRefreshLayout,swipeRefreshLayoutEmptyView;[m
[32m+[m[32m    private SwipeRefreshLayout swipeRefreshLayout, swipeRefreshLayoutEmptyView;[m
     private String businessString;[m
     private List<Business> listOfBusiness = new ArrayList<>();[m
     private RecyclerView recyclerViewBusiness;[m
     private RecyclerView.LayoutManager mLayoutManager;[m
 [m
     @Override[m
[31m-    public void onCreate(Bundle savedInstanceState){[m
[32m+[m[32m    public void onCreate(Bundle savedInstanceState) {[m
         super.onCreate(savedInstanceState);[m
         setContentView(R.layout.activity_home);[m
[31m-        recyclerViewBusiness = (RecyclerView)findViewById(R.id.recyclerViewBusiness);[m
[32m+[m[32m        recyclerViewBusiness = (RecyclerView) findViewById(R.id.recyclerViewBusiness);[m
         addBusinessFAB = (FloatingActionButton) findViewById(R.id.fabAddBusiness);[m
[31m-        swipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.swipe_refresh_layout);[m
[32m+[m[32m        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);[m
         swipeRefreshLayoutEmptyView = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout_emptyView);[m
         addBusinessFAB.setOnClickListener(new View.OnClickListener() {[m
             @Override[m
[36m@@ -84,7 +84,7 @@[m [mpublic class HomeActivity extends AppCompatActivity implements SwipeRefreshLayou[m
         mLayoutManager = new LinearLayoutManager(this);[m
         recyclerViewBusiness.setLayoutManager(mLayoutManager);[m
         pref = PreferenceManager.getDefaultSharedPreferences(this);[m
[31m-        String emailId = pref.getString(Commons.EMAIL,"sandeepkumarsingh.1993@gmail.com");[m
[32m+[m[32m        String emailId = pref.getString(Commons.EMAIL, "sandeepkumarsingh.1993@gmail.com");[m
         credential = GoogleAccountCredential.usingAudience(this,[m
                 "server:client_id:454280588675-33ro5ieerek0mcfgkubrk7aqpfbivjge.apps.googleusercontent.com");[m
         credential.setSelectedAccountName(emailId);[m
[36m@@ -98,15 +98,7 @@[m [mpublic class HomeActivity extends AppCompatActivity implements SwipeRefreshLayou[m
             }[m
         });[m
 [m
[31m-        swipeRefreshLayoutEmptyView.post(new Runnable(){[m
[31m-            @Override[m
[31m-             public void run(){[m
[31m-                swipeRefreshLayoutEmptyView.setRefreshing(true);[m
[31m-                new GetBusinessTask().execute();[m
[31m-            }[m
[31m-        });[m
[31m-        googleApiClient = new GoogleApiClient.Builder(this)[m
[31m-                            .addApi(Plus.API).addScope(Plus.SCOPE_PLUS_LOGIN).build();[m
[32m+[m[32m        createClient();[m
     }[m
 [m
     protected void onStop() {[m
[36m@@ -121,22 +113,24 @@[m [mpublic class HomeActivity extends AppCompatActivity implements SwipeRefreshLayou[m
         getMenuInflater().inflate(R.menu.menu_home, menu);[m
         return true;[m
     }[m
[32m+[m
     @Override[m
[31m-    public boolean onOptionsItemSelected(MenuItem item){[m
[32m+[m[32m    public boolean onOptionsItemSelected(MenuItem item) {[m
         int id = item.getItemId();[m
         SharedPreferences.Editor editor = pref.edit();[m
[31m-        if(id ==R.id.action_logout)[m
[31m-        {[m
[31m-//          Plus.AccountApi.clearDefaultAccount(googleApiClient);[m
[31m-            googleApiClient.clearDefaultAccountAndReconnect();[m
[31m-            googleApiClient.disconnect();[m
[32m+[m[32m        if (id == R.id.action_logout) {[m
[32m+[m[32m            if (googleApiClient.isConnected()) {[m
[32m+[m[32m                Plus.AccountApi.clearDefaultAccount(googleApiClient);[m
[32m+[m[32m//              googleApiClient.clearDefaultAccountAndReconnect();[m
[32m+[m[32m                googleApiClient.disconnect();[m
[32m+[m[32m            }[m
             editor.putString(Commons.USER_NAME, null);[m
             editor.putString(Commons.EMAIL, null);[m
             editor.putString(Commons.PIC_URL, null);[m
             editor.putString(Commons.GENDER, null);[m
             editor.putBoolean(Commons.HAS_USER_LOGGED_INTO_GOOGLE_PLUS, false);[m
             editor.commit();[m
[31m-            Intent intent = new Intent(HomeActivity.this,LoginActivity.class);[m
[32m+[m[32m            Intent intent = new Intent(HomeActivity.this, LoginActivity.class);[m
             intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);[m
             finish();[m
             startActivity(intent);[m
[36m@@ -144,32 +138,37 @@[m [mpublic class HomeActivity extends AppCompatActivity implements SwipeRefreshLayou[m
         return true;[m
     }[m
 [m
[31m-@Override[m
[31m-public void onBackPressed() {[m
[31m-    if (doubleBackToExitPressedOnce) {[m
[31m-        super.onBackPressed();[m
[31m-        finish();[m
[31m-        return;[m
[31m-    }[m
[32m+[m[32m    @Override[m
[32m+[m[32m    public void onBackPressed() {[m
[32m+[m